package io.gnews.pro.crawler.model.rest;

public enum Sentiment {

	POSITIVE,
	NEGATIVE,
	NEUTRAL
	
}

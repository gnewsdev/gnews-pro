package io.gnews.pro.crawler.service;

import java.util.Date;

import io.gnews.pro.core.model.mongodb.Project;

public interface CrawlerService {
	
	public void updateData(Project project, Date start, Date end, String typeUpdate);
	
	public void updateData(Project project);

	public void updateProjectData(Project project, Date start, Date end);

	public void updateKeywordMappingData(Project project, Date start, Date end);

	public Project getById(String id);

	public void updateAllProjectOnlyKM();

	public void updateSpesificKM(Project project, Date start, Date end);

	public void unUpdatedKeywordMapping();

	public void updateAllProjects(String typeUpdate);

	
}

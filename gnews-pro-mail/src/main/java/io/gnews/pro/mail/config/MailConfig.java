package io.gnews.pro.mail.config;

import java.security.GeneralSecurityException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import com.sun.mail.util.MailSSLSocketFactory;

@Configuration
public class MailConfig {

	@Value("${smtp.host}")
	private String host;
	@Value("${smtp.port}")
	private Integer port;
	@Value("${smtp.protocol}")
	private String protocol;
	@Value("${smtp.username}")
	private String username;
	@Value("${smtp.password}")
	private String password;

	@Bean
	public ClassLoaderTemplateResolver emailTemplateResolver() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setPrefix("mail/");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setOrder(1);
		return templateResolver;
	}

	@Bean
	public JavaMailSenderImpl javaMailSenderImpl()
			throws GeneralSecurityException {
		JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
		mailSenderImpl.setHost(host);
		mailSenderImpl.setPort(port);
		mailSenderImpl.setProtocol(protocol);
		mailSenderImpl.setUsername(username);
		mailSenderImpl.setPassword(password);
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true);

		Properties javaMailProps = new Properties();
		javaMailProps.put("mail.smtp.auth", true);
		javaMailProps.put("mail.smtp.starttls.enable", true);
		javaMailProps.put("mail.smtp.tls.enable", true);
		javaMailProps.put("mail.smtp.ssl.enable", true);
		javaMailProps.put("mail.smtp.ssl.socketFactory", sf);
		mailSenderImpl.setJavaMailProperties(javaMailProps);

		return mailSenderImpl;
	}

	@Bean
	public TemplateEngine mailTemplateEngine() {
		TemplateEngine engine = new TemplateEngine();
		engine.addTemplateResolver(emailTemplateResolver());
		return engine;
	}

}

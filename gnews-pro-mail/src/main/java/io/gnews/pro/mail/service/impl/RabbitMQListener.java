package io.gnews.pro.mail.service.impl;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AddARMailSenderForm;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.mail.service.EmailService;

@Component
public class RabbitMQListener {

	@Autowired
	private EmailService emailService;
	
	@RabbitListener(queues  = RabbitMQConfig.SEND_MAIL_QUEUE)
	public void sendMail(@Payload SendEmailForm form){
		emailService.sendEmail(form);
	}
	
	@RabbitListener(queues  = RabbitMQConfig.SEND_MAIL_AR)
	public void sendMailAR(@Payload AddARMailSenderForm form){
		emailService.sendARMail(form.getTo(), 
				form.getSubject(), 
				form.getDocName(), 
				form.getAttachment());
	}
	
}

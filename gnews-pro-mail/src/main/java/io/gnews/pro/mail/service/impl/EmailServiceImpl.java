package io.gnews.pro.mail.service.impl;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.base.Throwables;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import io.gnews.pro.core.model.dto.MailType;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.mail.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService {
	
	private static String REGISTER_SUBJECT ="Thank you for registering GNEWS Pro";
	private static String WELCOME_SUBJECT ="Welcome to GNEWS Pro";
	private static String INQUIRY_SUBJECT ="GNEWS Pro Inquiry";
	private static String CREATE_PROJECT_SUBJECT = "Your project has been created";
	private static String INVOICE_SUBJECT = "GNEWS Pro Invoice";
	private static String VALIDATE_EMAIL_SUBJECT = "Validate your email";
	private static String GNEWS_LETTER_SUBJECT = "GNEWS Letter %s";
	
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private JavaMailSender mailSender;

	@Value("${smtp.username}")
	private String username;

	@Value("${smtp.displayname}")
	private String displayName;

	@Autowired
	@Qualifier("mailTemplateEngine")
	private TemplateEngine templateEngine;

	@Override
	public void sendEmail(SendEmailForm form) {
		try {
			Gson gson = generateGSON();
			Map<String, Object> map = form.getData();
			Context ctx = new Context();
			ctx.setVariables(map);
			String subject = "";
			String content = "";
			if(form.getType() == (MailType.REGISTER)){
				subject = REGISTER_SUBJECT;
				content = templateEngine.process("", ctx);
				//sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.WELCOME)){
				subject = WELCOME_SUBJECT;
				content = templateEngine.process("", ctx);
				//sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.INQUIRY)){
				subject = INQUIRY_SUBJECT;
				content = templateEngine.process("inquiries-mail.html", ctx);
				sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.CREATE_PROJECT)){
				subject = CREATE_PROJECT_SUBJECT;
				content = templateEngine.process("", ctx);
				//sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.INVOICE)){
				String jsonArticles = gson.toJson(map.get("invoiceDate"));
				@SuppressWarnings("serial")
				Type type = new TypeToken<Date>() {
				}.getType();
				Date article = gson.fromJson(jsonArticles, type);
				map.replace("invoiceDate", article);
				ctx.setVariables(map);
				subject = INVOICE_SUBJECT;
				content = templateEngine.process("invoice.html", ctx);
				sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.VALIDATE)){
				subject = VALIDATE_EMAIL_SUBJECT;
				content = templateEngine.process("sender-validation.html", ctx);
				sendEmail(form.getTo(), subject, content, null);
				return;
			}
			if(form.getType() == (MailType.GNEWS_LETTER)){
				String jsonArticles = gson.toJson(map.get("articles"));
				@SuppressWarnings("serial")
				Type listType = new TypeToken<ArrayList<WebData>>() {}.getType();
                List<WebData> articles = gson.fromJson(jsonArticles, listType);	
                map.replace("articles", articles);
                ctx.setVariables(map);
    			subject = String.format(GNEWS_LETTER_SUBJECT, map.get("keyword"));
				content = templateEngine.process("gnews-mail-digest.html", ctx);
				sendEmail(form.getTo(), subject, content, null);
				return;
			}
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		} 
	}
	
	private void sendEmail(final String to, final String subject, final String content, final List<String> attachments) {
		try {
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(
							mimeMessage, true);
					message.setTo(new InternetAddress(to));
					message.setFrom(new InternetAddress(username, displayName));
					message.setSubject(subject);
					message.setText(content, true);
					if(attachments != null){
						for(String attachment : attachments){
							File file = new File(attachment);
							message.addAttachment(file.getName(), file);
						}
					}
				}
			};
			this.mailSender.send(preparator);
			log.info("email to " + to + " with subject, " + subject + " sent.");
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
	}
	
	@Override
	public void sendARMail(String to, String subject, String docName, String attachment) {
		try {
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				@Override
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
					message.setTo(new InternetAddress(to));
					message.setText("");
					log.info("true?? "+to);
					message.setFrom(new InternetAddress(username, displayName));
					message.setSubject(subject);
					File file = new File(attachment);
					log.info("null???" + file.getName());
					message.addAttachment(docName, file);
				}
			};
			this.mailSender.send(preparator);
			log.info("email to " + to + " with subject, " + subject + " sent.");
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}	
	}
	
	private Gson generateGSON(){
		GsonBuilder builder = new GsonBuilder(); 
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

			@Override
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				return new Date(json.getAsLong());
			}
		});
		
		return builder.create();
	}
}

package io.gnews.pro.letter.service;

import java.util.List;

import io.gnews.pro.core.model.mongodb.GnewsLetter;

public interface GnewsLetterService {

	public void sendSummary(int hour);

	public void sendSummary(
			String keyword, 
			String filter,
			String languange,
			String to,
			int days);
	
	public List<GnewsLetter> gnewsLetterActiveProjectChecker();

}

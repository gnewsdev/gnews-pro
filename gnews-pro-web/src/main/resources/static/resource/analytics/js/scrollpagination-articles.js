/*
 **	Anderson Ferminiano
 **	contato@andersonferminiano.com -- feel free to contact me for bugs or new implementations.
 **	jQuery ScrollPagination
 **	28th/March/2011
 **	http://andersonferminiano.com/jqueryscrollpagination/
 **	You may use this script for free, but keep my credits.
 **	Thank you.
 */
(function($) {
	
	var startDate = '';
	var endDate = '';
	var term = '';
		
	if($('#startDates').text().length == 0) {
		startDate = moment().subtract(7, 'days').format('YYYY-MM-DD');
		endDate = moment().format('YYYY-MM-DD');			
	} else {
		startDate = $('#startDates').text();
		endDate = $('#endDates').text();
	}
	
	$.fn.scrollPagination = function(
		options) {
		var settings = {
			nop: 10,
			offset: 10,
			speed: 100,
			error: 'No More Posts!',
			delay: 300,
			scroll: false,
			url: $('#apiReport').attr('href') + $('#projectID').text() + "/keyword-mapping/article?accessToken=" + $('#access_token').text() + "&start=" + startDate + "&end=" + endDate + "&limit=10&term="
		}
		
		return this.each(function() {

			$this = $(this);
			$settings = settings;
			var offset = $settings.offset;
			var url = $settings.url;
			var busy = false;
			var content = '';

			function getData() {
				term = $('#terem').text();
				$.ajax({
					type: "GET",
					url: url+term+"&offset="+offset,
					beforeSend: function() {
						
					},
					success: function(json) {
						var x = 0;
						var content = '';
						$.each(json.data, function(i, item){
							function setElement(){
								var url = '';
								var title = '';
								var host= '';
								var date= '';
								
								content =
				        			'<div class="col-sm-12" style="padding-left: 10px;">'+
										'<p class="header-item">'+
											'<a href="'+item.url+'" target="_blank">'+item.title+'</a>'+
										'</p>'+
										'<p class="footer-item">'+
											'<a href="'+item.url+'" target="_blank">'+item.host+'&nbsp;'+'<span class="newsdate">' +'</br>' +moment.unix(item.date).format('MMM DD, YYYY HH:mm')+'</span>'+'</a>'+
										'</p>'+
									'</div>';
							}
							$(setElement(this));
							$('#articles').append(content);
		  	            });
					},
					complete: function() {
						
					},
					failure: function(data) {}
				});
				offset = offset + $settings.nop;
				busy = false;
			}
			
			var load = '<div style="text-align: center;"">'+
			 '<a id="more" class="btn btn-keyword waves-effect" onClick="(50);">Load more</a>'+
			 '</div>';
			
			if ($settings.scroll == false) {
				$(window).scroll(function() {
					if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight)
					{
						busy = true;
						setTimeout(function() {
							getData();
							console.log("yay!");
						}, $settings.delay);
					}
				});
			}
		});
	}
})(jQuery);
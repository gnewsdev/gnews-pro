$(function () {
    $('#topten-chart').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
                text: 'Top 10 Media'
            },
        colors: ['#00A0DC'],
        xAxis: {
            categories: ['Kompas.com', 'Detik.com', 'MetroTVnews.co.io', 'Tempo.co', 'BeritaSatu.co.id', 'VivaNews.co.id', 'LoremIpsum.com', 'Lorem.net', 'Ipsumnews.co.id', 'lorsum.com']
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            floating: true,
            backgroundColor: '#FFFFFF',
            align: 'right',
            verticalAlign: 'top',
            y: 60,
            x: -60
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.x + ': ' + this.y;
            }
        },
        series: [{
            name: 'Number of news',
            data: [3, 4, 2, 7, 5, 9, 6, 9, 9, 5]
        }]
    });
});
/*<![CDATA[*/
var startDate = '';
var endDate = '';
var term = '';
var currentTerm = '';
var timestamp = '';
var day = '';
var hour = '';
var currentSentiment = '';
var loadedArticles = 0;
var totalMedia = 0;
var loadArticlesPositive =0;
var loadArticlesNegative =0;
var loadArticlesNeutral =0;

if($('#startDates').text().length == 0) {
	startDate = moment().subtract(7, 'days').format('YYYY-MM-DD');
	endDate = moment().format('YYYY-MM-DD');			
} else {
	startDate = $('#startDates').text();
	endDate = $('#endDates').text();
	term = $('#term').text();
}
	
function articleCount(){
	$.ajax({
	      type: "GET",
          url: addrArticleCount,                                
          data: "",
      	  dataType: "json",
       	  cache: false,
      	  beforeSend : function(){
      	  },
          success : function(data){
          },complete : function(data){   	        	
        	  var obj = JSON.parse(data.responseText);
        	  var values = $.number(obj.data);
        	  $('.post').number(values);
        	  }
	   });
}

function mediaCount(){
	$.ajax({
	      type: "GET",
         url: addrMediaCount,                                
         data: "",
     	  dataType: "json",
      	  cache: false,
     	  beforeSend : function(){
     	  },
         success : function(data){
         },complete : function(data){
        	 var obj = JSON.parse(data.responseText);
        	 var value = $.number(obj.data);
        	 $('#contributor').number(value);
        	 }
	   });
}

$(document).ready(function(){
	articleCount();
	mediaCount();
	$('.post').number();
	$('#contributor').number();
	
	$('input[name="daterange"]').daterangepicker(
			{
			    locale: {
			      format: 'YYYY-MM-DD'
			    },
			    maxDate: moment()
			}, 
			function(start, end, label) {
			    startDate = start.format('YYYY-MM-DD');
			    endDate = end.format('YYYY-MM-DD');
	});
	
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = $('body');
		offset = element.offset();
		offsetTop = offset.top;
		$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}
    
    $(function(){		 
		$(document).on( 'scroll', function(){		 
			if ($(window).scrollTop() > 300) {
				$('.scroll-top-wrapper').addClass('show');;
			} else {
				$('.scroll-top-wrapper').removeClass('show');
			}
		});
		$('.scroll-top-wrapper').on('click', scrollToTop);
		$('.fa-chevron-up').on('click', scrollToTop);
		$('.fade').addClass('animated fadeInUp');
	});
});


/*]]>*/
package io.gnews.pro.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Throwables;

import io.gnews.pro.web.request.InquiriesForm;
import io.gnews.pro.web.request.RegistrationForm;
import io.gnews.pro.web.service.MailMarketingService;
import io.gnews.pro.web.service.SiteService;
import io.gnews.pro.web.service.UserService;

@Controller
@RequestMapping(value = "/")
public class DefaultController {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private MailMarketingService mailMarketingService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(
			HttpSession session, 
			Principal principal,
			Model model){
		if (principal == null){
			
			siteService.setSiteInfo(principal,model);
			return "index";
		}
		log.info("logged in : " + principal.getName());
		return "redirect:/project";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "dashboard")
	public String dashboard(
			HttpSession session, 
			Principal principal) {
		
		if (principal == null) {
			return "redirect:/";
		}
		
		return "pro-dashboard";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "teams")
	public String teams(
			HttpSession session, 
			Principal principal) {
		
		if (principal == null) {
			return "redirect:/";
		}
		
		return "pro-teams";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "teamprojects")
	public String teamproject(
			HttpSession session, 
			Principal principal) {
		
		if (principal == null) {
			return "redirect:/";
		}
		
		return "pro-team-projects";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/inquiry/save")
	public String add(@ModelAttribute InquiriesForm Form) {
		userService.reuestInquiry(Form);
		return "thanks";
	}

	@RequestMapping(method = RequestMethod.GET, value = "reset-password")
	public String resetPassword(Principal principal){
		if(principal == null){
			return "reset-password";
		}
		return "redirect:/project";
	}

	@RequestMapping(method = RequestMethod.GET, value = "signin")
	public String signin(Principal principal){
		if(principal == null){
			return "index";
		}
		return "redirect:/project";
	}

	@RequestMapping(method = RequestMethod.GET, value = "signup")
	public String signup(Principal principal){
		if(principal == null){
			return "signup";
		}
		return "redirect:/project";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "signup")
	public String save(Principal principal, Model model, @ModelAttribute RegistrationForm form){
		try {
			userService.register(principal, form);
			return "register-success";
		} catch (Exception e) {
			model.addAttribute("error", e.getMessage());
			return "signup";
		}
	}

	@RequestMapping(value = "/mail/unsubscribe/{messageId}/{recipientId}", method = RequestMethod.GET)
	public String emailGroupRecipientUnsubscribe(
			@PathVariable String messageId, 
			@PathVariable String recipientId, 
			Model model,
			HttpSession session, 
			HttpServletRequest request, 
			HttpServletResponse response, 
			Principal principal) {
		try {
			siteService.setSiteInfo(principal, model);
			mailMarketingService.unsubscribeReceipient(request, messageId, recipientId);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "unsubscribed";
	}

}

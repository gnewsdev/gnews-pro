package io.gnews.pro.web.service;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpSession;

import io.gnews.pro.core.model.mongodb.GnewsLetter;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.web.request.GnewsLetterForm;
import io.gnews.pro.web.request.ProjectForm;
import io.gnews.pro.web.response.ProjectData;

public interface ProjectService {
	
	public List<Project> getUserProjects(Principal principal, String type);
	
	public Project getById(String id);

	public Project createProject(Principal principal, ProjectForm form, HttpSession session);
	
	public boolean deleteProject(String id);

	public void activate(String id, int i, boolean newProject);
	
	public List<ProjectData> CountTotalDays(Principal principal);
	
	public GnewsLetter saveGnewsLetter(Principal principal, GnewsLetterForm form);

	public List<GnewsLetter> getGnewsLetterUser(String id);
	
	public void deleteGnewsLetterUser(String id);

	public void saveGnewsLetterConfig(Principal principal, GnewsLetterForm form);
	

}

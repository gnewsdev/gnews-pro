package io.gnews.pro.web.request;

import org.springframework.web.multipart.MultipartFile;

public class CreateGroupForm {

	private String name;
	private String description;
	private MultipartFile file;
	private String emailReceiver;
	
	public CreateGroupForm() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getEmailReceiver() {
		return emailReceiver;
	}

	public void setEmailReceiver(String emailReceiver) {
		this.emailReceiver = emailReceiver;
	}
	
}

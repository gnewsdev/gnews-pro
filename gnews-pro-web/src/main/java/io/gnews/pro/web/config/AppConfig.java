package io.gnews.pro.web.config;

public class AppConfig {

	public static final String PROJECT_TYPE_GRID = "grid";
	public static final String PROJECT_TYPE_LIST = "list";
	
	public static final String PROJECT_FILTER_ACTIVE = "active";
	public static final String PROJECT_FILTER_EXPITED = "expired";
	public static final String PROJECT_FILTER_EXPIRED_SOON = "expired-soon";
	
	public static final String SESSION_DATA_NAME = "fullname";
	public static final String SESSION_DATA_EMAIL = "email";
	public static final String SESSION_DATA_AVATAR = "profilepicture";
	public static final String SESSION_PROJECT_TYPE = "projectType";
	
	public static final String SESSION_DATA_PROJECT_ID = "projectId";
	public static final String SESSION_DATA_PAYMENT_ID = "paymentId";
	public static final String SESSION_DATA_PROJECT_TYPE = "projectType";
	
	public static final String EMERGENCY_UPDATE_KM = "updating keyword mapping for all project";
	public static final String EMERGENCY_UPDATE_ALL_PROJECT = "updating all project";
	public static final String EMERGENCY_UPDATE_SELECTED_KM = "updateing spesific project";

	public static final String HEADER_AGENT = "User-Agent";
	public static final String HEADER_ACCEPT = "Accept";

	public static final long DEFAULT_PRICE = 0l;
	
	public static final int DEFAULT_ANALYTIC_DAY = 7;
	
	public static final int DEFAULT_RECURRENT_PERIOD = 12;
	
	public static final String PAYMENT_METHOD_PAYPAL = "Paypal";

	public static final String PAYMENT_METHOD_BANK = "Bank Transfer";
	
	public static final String[] PAYMENT_METHOD = {PAYMENT_METHOD_PAYPAL, PAYMENT_METHOD_BANK};
	
	public static final String PAYPAL_CURRENCY = "IDR";
	
	public static final String CRAWLING_PENDING = "Pending";
	
	public static final String CRAWLING_FINISH = "Finish";
	
	public static final String[] COUNTRY = {"en","id"};
	
	public static final String STATIC_ACCESSTOKEN = "aHCl8LjxTPAE3bsLcLM0";
	
}

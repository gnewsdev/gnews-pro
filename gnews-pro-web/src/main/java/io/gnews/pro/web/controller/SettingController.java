package io.gnews.pro.web.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Throwables;

import io.gnews.pro.web.request.ChangePasswordForm;
import io.gnews.pro.web.request.ProfileForm;
import io.gnews.pro.web.response.ProfileData;
import io.gnews.pro.web.service.SiteService;
import io.gnews.pro.web.service.UserService;

@Controller
@RequestMapping("/setting")
public class SettingController {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SiteService siteServices;
	
	@Autowired
	private UserService settingService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String editProfile(HttpSession session,Principal principal, Model model){
		if(principal == null){
			return "redirect:/";
		}
		
		siteServices.setSiteInfo(principal,model);
		
		ProfileData profileData = settingService.getUserData(principal);
		model.addAttribute("profile", profileData);
		
		return "user/account-setting";
	}
		

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute ProfileForm Form,
			@RequestParam("image") MultipartFile image) {
		if (principal == null) {
			return "redirect:/sigin";
		}
		settingService.save(principal, Form,image);
		return "redirect:/setting";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/password/save")
	public String passChanger(Principal principal, @ModelAttribute ChangePasswordForm form, Long id){
		if (principal == null) {
			return "redirect:/";
		}
		try {
			settingService.savePassword(form, principal);
		} catch (Exception e) {
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/setting";
	}
	
	
}
	

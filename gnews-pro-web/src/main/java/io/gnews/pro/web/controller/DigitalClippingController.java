package io.gnews.pro.web.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Throwables;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.util.DateUtils;
import io.gnews.pro.web.config.AppConfig;
import io.gnews.pro.web.request.DigitalClippingForm;
import io.gnews.pro.web.service.DigitalClippingService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.SiteService;

@Controller
@RequestMapping(value="/project")
public class DigitalClippingController {
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private SiteService siteService;

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private DigitalClippingService clippingService;
	
	@RequestMapping(value = "/{id}/digital-clipping/start")
	public String clippingstart(
			@PathVariable String id, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			model.addAttribute("project", projectService.getById(id));
			siteService.setSiteInfo(principal, model);
			return "digital-clipping/digital-clipping-start";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(value = "/{id}/digital-clipping")
	public String clipping(
			@PathVariable String id, 
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date start,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date end, 
			Model model, 
			HttpSession session, 
			HttpServletRequest request,
			HttpServletResponse response,
			Principal principal) {
		try{
			if(end == null){
				end = new Date();
			}
			if(start == null){
				start = DateUtils.before(end, AppConfig.DEFAULT_ANALYTIC_DAY);
			}
			
			model.addAttribute("start", dateFormat.format(start));
			model.addAttribute("end", dateFormat.format(end));
			model.addAttribute("project", projectService.getById(id));
			siteService.setSiteInfo(principal, model);
			
			return "digital-clipping/digital-clipping";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project";
	}
	
	@RequestMapping(method = RequestMethod.POST, value ="/digital-clipping/save/news")
	public String saveArticleNews(
			Principal principal,
			Model model,
			@ModelAttribute DigitalClippingForm form
			){
		if(principal == null){
			return null;
		}
		try{
			Project project = projectService.getById(form.getIdProject());
			clippingService.saveNewUrlDC(project, form);
			return "redirect:/project/"+form.getIdProject()+"/digital-clipping";
			
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/"+form.getIdProject()+"/digital-clipping";
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value="/digital-clipping/delete")
	public String deleteArticle(
			Principal principal,
			Model model,
			@RequestParam(required = false) String urlId,
			@RequestParam(required = false) String idProject
			){
		if(principal == null){
			return "redirect:/project";
		}
		try{
			log.info("accessed");
			Project project = projectService.getById(idProject);
			clippingService.deleteArticleDC(project, urlId);
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			
		}
		return "redirect:/project/"+idProject+"/digital-clipping";
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/digital-clipping/edit")
	public String editArticle(
			Principal principal,
			Model model,
			@RequestParam(required = false) String urlId,
			@RequestParam(required = false) String idProject
			){
		if(principal == null){
			return "redirect:/project";
		}
		try{
			log.info("accessed");
			Project project = projectService.getById(idProject);
			ArticleData articleData = clippingService.editArticleDC(project, urlId);
			model.addAttribute("articleData", articleData);
			
			return "redirect:/project/"+idProject+"/digital-clipping";
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
			
		}
		return "redirect:/project/"+idProject+"/digital-clipping";
	}
	
	@RequestMapping(method = RequestMethod.POST, value ="/digital-clipping/save")
	public String saveArticle(
			Principal principal,
			Model model,
			@ModelAttribute DigitalClippingForm form
			){
		if(principal == null){
			return null;
		}
		try{
			Project project = projectService.getById(form.getIdProject());
			clippingService.saveArticleDC(project, form.getUrlId(), form);
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
		return "redirect:/project/"+form.getIdProject()+"/digital-clipping";
	}
	
	
	

}
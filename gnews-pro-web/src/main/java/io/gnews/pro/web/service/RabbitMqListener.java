package io.gnews.pro.web.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.MessageSimple;

@Component
public class RabbitMqListener {

	@Autowired
	private MailMarketingService mailMarketingService;

	@RabbitListener(queues  = RabbitMQConfig.SAVE_MAIL_BLAST_QUEUE)
	public void createProject(@Payload MessageSimple message){
		mailMarketingService.saveMailBlast(message);
	}
}


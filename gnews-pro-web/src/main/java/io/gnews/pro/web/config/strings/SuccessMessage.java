package io.gnews.pro.web.config.strings;

/**
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public class SuccessMessage {

	public static final String SUCCESS = "success";
	public static final String OK = "ok";
	public static final String HEALTH = "health";
	public static final String DEFAULT_SAVE_SUCCESS = "Successfully updated your request.";
	
}

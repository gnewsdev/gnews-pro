package io.gnews.pro.web.service.impl;

import java.io.IOException;
import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.base.Throwables;

import io.gnews.pro.core.model.dto.AutomaticReportForm;
import io.gnews.pro.core.model.mongodb.AutomatedReport;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.AutomatedReportRepository;
import io.gnews.pro.core.repository.mongodb.ProjectRepository;
import io.gnews.pro.web.service.AutomatedReportService;
import io.gnews.pro.web.service.UploadService;

@Service
public class AutomatedReportServiceImpl implements AutomatedReportService {

	@Value("${report.data.companylogo}")
	private String companyLogoDir;

	private Logger log = LoggerFactory.getLogger(getClass());

	public static final int IMAGE_SCALE_WIDTH = 280;
	public static final int IMAGE_SCALE_HEIGHT = 150;

	@Autowired
	private AutomatedReportRepository automatedReportRepository;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private ProjectRepository projectRepository;

	// AutomatedReport Configuration
	@Override
	public AutomatedReport saveConfiguration(Principal principal, AutomaticReportForm form, MultipartFile imageUpload) {
		Project project = projectRepository.findOne(form.getId());
		AutomatedReport automated = automatedReportRepository.findByProject(project);
		if (automated != null) {
			automated.setActive(true);
			automated.setEmail(form.getEmail());
			automated.setHour(form.getHour());
			automated.setReportType(form.getReportType());
			if (!automated.getDay().equals(form.getDay())) {
				automated.setDay(form.getDay());
				automated.setMonthlyCount(0);
			}
			if (!imageUpload.isEmpty()) {
				try {
					String image = uploadService.uploadCompanyLogo(companyLogoDir, project.getId(),
							imageUpload.getOriginalFilename(), imageUpload, IMAGE_SCALE_WIDTH, IMAGE_SCALE_HEIGHT);
					if (image != null) {
						automated.setCompanyLogo(project.getId() + image);
						log.info("image change : " + image);
					}
				} catch (IOException e) {
					log.error(Throwables.getRootCause(e).getMessage());
				}
			}
			return automatedReportRepository.save(automated);
		} else {
			AutomatedReport automatedReport = new AutomatedReport();
			automatedReport.setEmail(form.getEmail());
			automatedReport.setDay(form.getDay());
			automatedReport.setHour(form.getHour());
			automatedReport.setReportType(form.getReportType());
			automatedReport.setMonthlyCount(0);
			if (!imageUpload.isEmpty()) {
				try {
					String image = uploadService.uploadCompanyLogo(companyLogoDir, project.getId(),
							imageUpload.getOriginalFilename(), imageUpload, IMAGE_SCALE_WIDTH, IMAGE_SCALE_HEIGHT);
					if (image != null) {
						automatedReport.setCompanyLogo(project.getId() + image);
						log.info("image change : " + image);
					}
				} catch (IOException e) {
					log.error(Throwables.getRootCause(e).getMessage());
				}
			}
			automatedReport.setActive(true);
			return automatedReportRepository.save(automatedReport);
		}
	}

	@Override
	public AutomatedReport getByProjectId(String id) {
		return automatedReportRepository.findByProjectId(id);
	}

}

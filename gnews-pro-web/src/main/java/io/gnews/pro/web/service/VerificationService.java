package io.gnews.pro.web.service;

import javax.servlet.http.HttpServletRequest;

import io.gnews.pro.core.model.mongodb.EmailSender;

public interface VerificationService {

	public boolean validateEmailSender(String token);

	public String generateSenderVerificationUrl(EmailSender emailSender, HttpServletRequest request);
	
}

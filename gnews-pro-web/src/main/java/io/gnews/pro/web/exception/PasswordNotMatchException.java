package io.gnews.pro.web.exception;

import io.gnews.pro.web.config.strings.ErrorMessage;

@SuppressWarnings("serial")
public class PasswordNotMatchException extends Exception {

	public PasswordNotMatchException() {
		super(ErrorMessage.PASSWORD_NOT_SAME_ERROR);
	}
	
}

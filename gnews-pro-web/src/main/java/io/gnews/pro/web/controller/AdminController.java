package io.gnews.pro.web.controller;

import java.util.Date;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.gnews.pro.core.config.AppConfig;
import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.AutomaticReportForm;
import io.gnews.pro.web.service.ProjectService;

@RestController
@RequestMapping("/admin/update")
public class AdminController {
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@RequestMapping(value= "/keywordmapping", method = RequestMethod.GET)
	@ResponseBody
	public String updateKeywordMapping(
			@RequestParam(required = true) String accessToken) {
		if(!accessToken.equals(AppConfig.STATIC_ACCESSTOKEN)){
			return null;
		}
		Long a = 1L;
		rabbitTemplate.convertAndSend(RabbitMQConfig.EMERGENCY_UPDATE_ALL_KM, a);
		
		return io.gnews.pro.web.config.AppConfig.EMERGENCY_UPDATE_KM;
	}
	@RequestMapping(value="/{id}/selectedkeyword")
	public String updateSelectedKM(
			@RequestParam(required = true) String accessToken,
			@PathVariable(value="id") String id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end){
		
		if(!accessToken.equals(AppConfig.STATIC_ACCESSTOKEN)){
			return null;
		}
		
		AutomaticReportForm form = new AutomaticReportForm();
		form.setId(id);
		form.setStart(start);
		form.setEnd(end);
		
		rabbitTemplate.convertAndSend(RabbitMQConfig.EMERGENCY_UPDATE_KM, form);
		
		return io.gnews.pro.web.config.AppConfig.EMERGENCY_UPDATE_SELECTED_KM;
	}
	
	@RequestMapping(value="/allproject", method = RequestMethod.GET)
	@ResponseBody
	public String udpateAllProject(@RequestParam(
			required=true) String accessToken){
		if(!accessToken.equals(AppConfig.STATIC_ACCESSTOKEN)){
			return null;
		}
		Long a = 1L;
		rabbitTemplate.convertAndSend(RabbitMQConfig.EMERGENCY_UPDATE_ALL_PROJECT, a);
		
		return io.gnews.pro.web.config.AppConfig.EMERGENCY_UPDATE_ALL_PROJECT;
	}
	@RequestMapping(value = "{id}/spesificproject", method = RequestMethod.GET)
	@ResponseBody
	public String updateSpesificProject(
			@RequestParam(required = true) String accessToken,
			@PathVariable(value="id") Long id, 
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date start,
    		@RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date end){
		
		if(!accessToken.equals(AppConfig.STATIC_ACCESSTOKEN)){
			return null;
		}
		
		AutomaticReportForm form = new AutomaticReportForm();
		form.setId(id);
		form.setStart(start);
		form.setEnd(end);
		rabbitTemplate.convertAndSend(RabbitMQConfig.EMERGENCY_PROJECT, form);
		
		return "update spesific project";
		
	}
		
	
}

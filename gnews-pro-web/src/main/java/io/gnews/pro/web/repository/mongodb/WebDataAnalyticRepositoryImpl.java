package io.gnews.pro.web.repository.mongodb;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.previousOperation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import io.gnews.pro.core.model.dto.ArticleData;
import io.gnews.pro.core.model.dto.PeakCounter;
import io.gnews.pro.core.model.dto.RankCounter;
import io.gnews.pro.core.model.dto.RankCounterMedia;
import io.gnews.pro.core.model.dto.SentimentCounter;
import io.gnews.pro.core.model.dto.SentimentPeakCounter;
import io.gnews.pro.core.model.dto.TermCounter;
import io.gnews.pro.core.model.mongodb.KeywordMapping;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.WebData;
import io.gnews.pro.core.model.service.AlexaRankService;
import io.gnews.pro.core.model.service.AlexaRankService.AlexaResult;
import io.gnews.pro.web.response.KeywordMappingData;

/**
 * @author masasdani
 * Created Date Nov 4, 2015
 */
@Repository
public class WebDataAnalyticRepositoryImpl implements WebDataAnalyticRepository {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public static final String BASE_COLLECTION = "web_data";  
	
	public static final String MEDIA_RANK_COLLECTION = "media_ranking"; 
	
	public static final String MEDIA_RANK_COLLECTION_POSITIVE = "media_ranking_positive"; 
	
	public static final String MEDIA_RANK_COLLECTION_NEGATIVE = "media_ranking_negative"; 
	
	public static final String MEDIA_RANK_COLLECTION_GLOBAL = "media_ranking_global";
	
	public static final String KEYWORD_MAPPING_COLLECTION = "keyword_mapping"; 
	
	public static final String KEYWORD_MAPPING_POSITIVE_COLLECTION = "keyword_mapping_positive";
	
	public static final String KEYWORD_MAPPING_NEGATIVE_COLLECTION = "keyword_mapping_negative";
	
	public static final String PEAKDAY_SENTIMENT_COLLECTION = "peak_day_sentiment";
	
	public static final String COLLECTION_SPARATOT = "_"; 
	
	private static final int MAX_RELATED_TERMS = 100;
	
	private static final int MAX_MEDIA_RANK_RESULT = 20;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private AlexaRankService alexaRankService;
	
	/**
	 * @param project
	 * @return
	 */
	private String getCollection(Project project){
		return BASE_COLLECTION + COLLECTION_SPARATOT + project.getId();
	}

	private String getCollection(String baseCollection, Project project){
		return baseCollection + COLLECTION_SPARATOT + project.getId();
	}
	
	private String getCollectionNew(String baseCollection){
		return baseCollection;
	}
	/**
	 * @param collectionName
	 */
	private void createCollectionIfNotExist(String collectionName){
		if(!mongoTemplate.collectionExists(collectionName)){
			mongoTemplate.createCollection(collectionName);
		}
	}
	
	@Override
	public void saveKeywordMappingData(Project project, Date startDate, Date endDate){
		log.info("start update keyword mapping data");
		createCollectionIfNotExist(getCollection(project));
		String map = "function () { "
						+ "for ( var i=0; i<this.terms.length; i++ ){ "
							+ "emit( this.terms[i] , 1 ); "
						+ "} "
					+ "}";
		String reduce = "function (key, values) { return values.length; }";
		
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		String outputCollection = getCollection(KEYWORD_MAPPING_COLLECTION, project);
		if(mongoTemplate.collectionExists(outputCollection)) {
			mongoTemplate.dropCollection(outputCollection);
		}
		createCollectionIfNotExist(outputCollection);
		mongoTemplate.mapReduce(query, getCollection(project), map, reduce, 
				new MapReduceOptions().outputCollection(outputCollection), TermCounter.class);
		log.info("keyword mapping updated");
	}
	
	@Override
	public void saveWebData(Project project, WebData webData) {
		mongoTemplate.save(webData, getCollection(project));
	}

	@Override
	public WebData getWebData(Project project, String url) {
		return mongoTemplate.findById(url, WebData.class, getCollection(project));
	}
	
	@Override
	public Date getLastNewsDate(Project project) {
		Query query = new Query();		
		query.fields().include("date");
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.limit(1);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		if(list.size() > 0){
			return list.get(0).getDate();
		}
		return null;
	}
	
	@Override
	public Long getMediaCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		
		String map = "function () { emit( this.domain , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";
		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();
	}
	
	
	@Override
	public Long getMediaMonthlyCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		
		String map = "function () { emit( this.domain , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";
		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();
	}
	

	@Override
	public Long getPeakHourMediaCount(Project project, Date startDate, Date endDate,
			Long peakHour) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));
		query.addCriteria(Criteria.where("peakHour").is(peakHour));		
		
		String map = "function () { emit( this.host , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";
		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();
	}

	@Override
	public Long getArticleCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		Long count = mongoTemplate.count(query, getCollection(project));
		return count;
	}
	

	@Override
	public Long getArticleMonthlyCount(Project project, Date startDate,
			Date endDate) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		Long count = mongoTemplate.count(query, getCollection(project));
		return count;
	}
	
	@Override
	public List<TermCounter> getKeywordMapping(Project project,
			Date startDate, Date endDate) {
		List<TermCounter> counters = new ArrayList<TermCounter>();
		String outputCollection = getCollection(KEYWORD_MAPPING_COLLECTION, project);
		if(!mongoTemplate.collectionExists(outputCollection) || mongoTemplate.count(new Query(), outputCollection) == 0){
            saveKeywordMappingData(project, startDate, endDate);
        }
		Query queryResult = new Query().limit(MAX_RELATED_TERMS);
		if(mongoTemplate.count(queryResult, outputCollection) < MAX_RELATED_TERMS) {
			queryResult.addCriteria(Criteria.where("value").gt(2));
		}
		queryResult.with(new Sort(Sort.Direction.DESC, "value"));
		counters = mongoTemplate.find(queryResult, TermCounter.class, outputCollection);
		return counters;
	}

	@Override
	public List<TermCounter> getKeywordMappingPositive(Project project,
			Date startDate, Date endDate) {
		List<TermCounter> counters = new ArrayList<TermCounter>();
		String outputCollection = getCollection(KEYWORD_MAPPING_POSITIVE_COLLECTION, project);
		if(!mongoTemplate.collectionExists(outputCollection)){
			saveKeywordMappingData(project, startDate, endDate);
		}
		Query queryResult = new Query().limit(MAX_RELATED_TERMS);
		if(mongoTemplate.count(queryResult, outputCollection) < MAX_RELATED_TERMS) {
			queryResult.addCriteria(Criteria.where("value").gt(2));
		}
		queryResult.with(new Sort(Sort.Direction.DESC, "value"));
		counters = mongoTemplate.find(queryResult, TermCounter.class, outputCollection);
		return counters;
	}

	@Override
	public List<TermCounter> getKeywordMappingNegative(Project project,
			Date startDate, Date endDate) {
		List<TermCounter> counters = new ArrayList<TermCounter>();
		String outputCollection = getCollection(KEYWORD_MAPPING_NEGATIVE_COLLECTION, project);
		if(!mongoTemplate.collectionExists(outputCollection)){
			saveKeywordMappingData(project, startDate, endDate);
		}
		saveKeywordMappingData(project, startDate, endDate);
		Query queryResult = new Query().limit(MAX_RELATED_TERMS);
		if(mongoTemplate.count(queryResult, outputCollection) < MAX_RELATED_TERMS) {
			queryResult.addCriteria(Criteria.where("value").gt(2));
		}
		queryResult.with(new Sort(Sort.Direction.DESC, "value"));
		counters = mongoTemplate.find(queryResult, TermCounter.class, outputCollection);
		return counters;
	}

	@Override
	public List<TermCounter> getMostMedia(
			Project project,
			Date startDate, 
			Date endDate, 
			int limit, 
			int offset) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)),	
				group("domain")
				.count().as("value"),
				sort(Sort.Direction.DESC, "value"),
				skip(offset),
				limit(limit)
		);
		AggregationResults<TermCounter> result = mongoTemplate
				.aggregate(aggregation, getCollection(project), TermCounter.class);		
		return result.getMappedResults();
	}

	@Override
	public List<ArticleData> getMostMediaByDomain(
			Project project, 
			String domain, 
			Date startDate, 
			Date endDate, 
			int limit,
			int offset) {
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));		
		query.addCriteria(Criteria.where("domain").in(domain));
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}
	
	@Override
	public List<TermCounter> getMostMediaNegative(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.NEGATIVE_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<TermCounter> getMostMediaPositive(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.POSITIVE_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<TermCounter> getMostMediaNeutral(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		return getMostMedia(project, WebData.NEUTRAL_SENTIMENT, 
				startDate, endDate, limit, offset);
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularMedia(Project project,
			Date startDate, Date endDate) {
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		String outputCollection = getCollection(MEDIA_RANK_COLLECTION, project);
		if(!mongoTemplate.collectionExists(outputCollection)){
			//should be sent to rabbitMQ and execute by crawler
			//updateMostPopularMedia(project, startDate, endDate);
		}
		Query queryResult = new Query().limit(MAX_MEDIA_RANK_RESULT);
		queryResult.with(new Sort(Sort.Direction.DESC, "value"));
		counters = mongoTemplate.find(queryResult, RankCounterMedia.class, outputCollection);
		return counters;
		
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularPositiveMedia(
			Project project, Date startDate, Date endDate, int limit) {
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		List<RankCounterMedia> counter = new ArrayList<RankCounterMedia>();
		
		List<TermCounter> list = getMostMediaPositive(project, startDate, endDate, limit, 0);
		
		for(TermCounter termCounter : list){
			DateTime dateTime = new DateTime(new Date()).dayOfMonth().roundFloorCopy().toDateTime();
			Query query = new Query(Criteria.where("date").is(dateTime));
			query.addCriteria(Criteria.where("domain").is(termCounter.getTerm()));
			counters = mongoTemplate.find(query, RankCounterMedia.class, 
					getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			if (counters.size() < 1) {
				AlexaResult result = alexaRankService.getResult(termCounter.getTerm());
				RankCounter rankCounter = new RankCounter();
				rankCounter.setDomain(termCounter.getTerm());
				rankCounter.setDate(dateTime);
				rankCounter.setCountryName(result.getCountryName());
				rankCounter.setCountryRank(result.getCountryRank());
				rankCounter.setGlobalRank(result.getPopularityRank());
				mongoTemplate.save(rankCounter, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			}
			
			RankCounter rCounter = mongoTemplate.findOne(query, RankCounter.class, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			RankCounterMedia rankCounterMedia = new RankCounterMedia();
			rankCounterMedia.setDomain(termCounter.getTerm());
			rankCounterMedia.setCountryName(rCounter.getCountryName());
			rankCounterMedia.setValue(termCounter.getValue());
			rankCounterMedia.setCountryRank(rCounter.getCountryRank());
			rankCounterMedia.setGlobalRank(rCounter.getGlobalRank());
			
			counter.add(rankCounterMedia);
		}
		return counter;
	}
	
	@Override
	public List<RankCounterMedia> getMostPopularNegativeMedia(
		Project project, Date startDate, Date endDate, int limit) {
		List<RankCounterMedia> counter = new ArrayList<RankCounterMedia>();
		List<RankCounterMedia> counters = new ArrayList<RankCounterMedia>();
		
		List<TermCounter> list = getMostMediaNegative(project, startDate, endDate, limit, 0);
		for(TermCounter termCounter : list){
			DateTime dateTime = new DateTime(new Date()).dayOfMonth().roundFloorCopy().toDateTime();
			Query query = new Query(Criteria.where("date").is(dateTime));
			query.addCriteria(Criteria.where("domain").is(termCounter.getTerm()));
			counters = mongoTemplate.find(query, RankCounterMedia.class, 
					getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			if (counters.size() < 1) {
				AlexaResult result = alexaRankService.getResult(termCounter.getTerm());
				RankCounter rankCounter = new RankCounter();
				rankCounter.setDomain(termCounter.getTerm());
				rankCounter.setDate(dateTime);
				rankCounter.setCountryName(result.getCountryName());
				rankCounter.setCountryRank(result.getCountryRank());
				rankCounter.setGlobalRank(result.getPopularityRank());
				mongoTemplate.save(rankCounter, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			}
			RankCounter rCounter = mongoTemplate.findOne(query, RankCounter.class, getCollectionNew(MEDIA_RANK_COLLECTION_GLOBAL));
			RankCounterMedia rankCounterMedia = new RankCounterMedia();
			rankCounterMedia.setDomain(termCounter.getTerm());
			rankCounterMedia.setCountryName(rCounter.getCountryName());
			rankCounterMedia.setValue(termCounter.getValue());
			rankCounterMedia.setCountryRank(rCounter.getCountryRank());
			rankCounterMedia.setGlobalRank(rCounter.getGlobalRank());
			
			counter.add(rankCounterMedia);
		}		
		return counter;
	}
	
	
	@Override
	public List<PeakCounter> getPeakDay(Project project,
			Date startDate, Date endDate) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)),	
				group("peakDay")
					.count().as("value"),
				sort(Sort.Direction.ASC, previousOperation(), "peakDay")
		);
		AggregationResults<PeakCounter> results = mongoTemplate
				.aggregate(aggregation, getCollection(project), PeakCounter.class);
		return results.getMappedResults();
	}

	@Override
	public List<PeakCounter> getPeakHour(Project project,
			Date startDate, Date endDate) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)),	
				group("peakHour")
					.count().as("value"),
				sort(Sort.Direction.DESC, previousOperation(), "peakHour")
		);
		AggregationResults<PeakCounter> results = mongoTemplate
				.aggregate(aggregation, getCollection(project), PeakCounter.class);
		return results.getMappedResults();
	}

	@Override
	public SentimentCounter getSentimentCounter(Project project,
			Date startDate, Date endDate) {
		SentimentCounter counter = new SentimentCounter();
		Query positiveQuery = new Query(Criteria.where("sentiment").is(WebData.POSITIVE_SENTIMENT));		
		positiveQuery.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		Query negativeQuery = new Query(Criteria.where("sentiment").is(WebData.NEGATIVE_SENTIMENT));
		negativeQuery.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		Query neutralQuery = new Query(Criteria.where("sentiment").is(WebData.NEUTRAL_SENTIMENT));
		neutralQuery.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		
		counter.setPositive(mongoTemplate.count(positiveQuery, getCollection(project)));
		counter.setNegative(mongoTemplate.count(negativeQuery, getCollection(project)));
		counter.setNeutral(mongoTemplate.count(neutralQuery, getCollection(project)));
		
		return counter;
	}
	
	@Override
	public List<ArticleData> getArticlePositive(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		Query positiveQuery = new Query(Criteria.where("sentiment").is(WebData.POSITIVE_SENTIMENT));		
		positiveQuery.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		positiveQuery.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(positiveQuery, WebData.class, getCollection(project));
		return toListArticle(list);
	}

	@Override
	public List<ArticleData> getArticleNegative(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		Query query = new Query(Criteria.where("sentiment").is(WebData.NEGATIVE_SENTIMENT));		
		query.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}

	@Override
	public List<ArticleData> getArticleNeutral(Project project,
			Date startDate, Date endDate, int limit, int offset) {
		Query query = new Query(Criteria.where("sentiment").is(WebData.NEUTRAL_SENTIMENT));		
		query.addCriteria(Criteria.where("date").gte(startDate).lte(endDate));
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}

	@Override
	public List<ArticleData> getArticleByPeakDay(Project project,
			Long peakDay, int limit, int offset) {
		Query query = new Query(Criteria.where("peakDay").is(peakDay));		
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}

	@Override
	public List<ArticleData> getArticleByPeakHour(Project project,
			Long peakHour, int limit, int offset) {
		Query query = new Query(Criteria.where("peakHour").is(peakHour));		
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}
	
	@Override
	public List<ArticleData> getArticleByTerm(Project project, String term, Date startDate, Date endDate,
			int limit, int offset) {
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));
		query.addCriteria(Criteria.where("terms").in(term));		
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}
	
	@Override
	public Long getArticleCountByTerm(Project project, String term, Date startDate,
			Date endDate) {		
		Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate)
				.andOperator(Criteria.where("terms").in(term)));
		Long count = mongoTemplate.count(query, getCollection(project));
		return count;
	}
	
	@Override
	public Long getMediaCountByTerm(Project project, String term, Date startDate, Date endDate){		
		Query query = new Query(Criteria.where("date").gt(startDate).lte(endDate).
				andOperator(Criteria.where("terms").in(term)));		
		String map = "function () { emit( this.domain , 1 ); }";
		String reduce = "function (key, values) { return values.length; }";		
		MapReduceResults<TermCounter> mapReduceResults = mongoTemplate
				.mapReduce(query, getCollection(project), map, reduce, TermCounter.class);
		return mapReduceResults.getCounts().getOutputCount();		
	}
	
	private List<ArticleData> toListArticle(List<WebData> list){
		List<ArticleData> articles = new ArrayList<ArticleData>();
		for(WebData data : list){
			ArticleData article = data.toArticleSimple();
			articles.add(article);
		}
		return articles;
	}
	
	public List<KeywordMappingData> toKeywordMapping(List<KeywordMapping> list){
		List<KeywordMappingData> keywordMappingDatas = new ArrayList<KeywordMappingData>();
		for(KeywordMapping keywordMapping : list){
			keywordMapping.setDate(keywordMapping.getDate());
			keywordMapping.setProjectId(keywordMapping.getProjectId());
			keywordMapping.setTermCounter(keywordMapping.getTermCounter());
		}
		return keywordMappingDatas;
	}
	
	@Override
	public void removeProject(Project project) {
		removeCollection(getCollection(project));
		removeCollection(getCollection(KEYWORD_MAPPING_COLLECTION, project));
		removeCollection(getCollection(KEYWORD_MAPPING_POSITIVE_COLLECTION, project));
		removeCollection(getCollection(KEYWORD_MAPPING_NEGATIVE_COLLECTION, project));
		removeCollection(getCollection(MEDIA_RANK_COLLECTION, project));
	}
	
	private List<TermCounter> getMostMedia(
			Project project, 
			int sentiment, 
			Date startDate, 
			Date endDate, 
			int limit, 
			int offset) {
		Aggregation aggregation = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate).andOperator(Criteria.where("sentiment").is(sentiment))),	
				group("domain")
				.count().as("value"),
				sort(Sort.Direction.DESC, "value"),
				skip(offset),
				limit(limit)
		);
		AggregationResults<TermCounter> result = mongoTemplate
				.aggregate(aggregation, getCollection(project), TermCounter.class);		
		return result.getMappedResults();
	}

	private void removeCollection(String collection){
		if(mongoTemplate.collectionExists(collection)){
			mongoTemplate.dropCollection(collection);
		}
	}
	
	@Override
	public List<ArticleData> getPeakDayById(Project project, String id, Date startDate, Date endDate) {
		Query query = new Query(Criteria.where("id").in(id));
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}
	
	@Override
	public List<SentimentPeakCounter> getSentimentPositiveEachDay(Project project, Date startDate, Date endDate) {
		Aggregation positiveQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.POSITIVE_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(positiveQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}

	@Override
	public List<SentimentPeakCounter> getSentimentNegativeEachDay(Project project, Date startDate, Date endDate) {
		Aggregation negativeQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.NEGATIVE_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(negativeQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}

	@Override
	public List<SentimentPeakCounter> getSentimentNeutralEachDay(Project project, Date startDate, Date endDate) {
		Aggregation neutralQuery = newAggregation(
				match(Criteria.where("date").gte(startDate).lte(endDate)
					.andOperator(Criteria.where("sentiment").is(WebData.NEUTRAL_SENTIMENT))
				),
				group("peakDay")
					.count().as("count"),
					sort(Sort.Direction.DESC, previousOperation(), "peakDay")
			);
		AggregationResults<SentimentPeakCounter> result = mongoTemplate
				.aggregate(neutralQuery, getCollection(project), SentimentPeakCounter.class);
		return result.getMappedResults();
	}

	@Override
	public List<ArticleData> getArticleSentimentByPeakDay(Project project, Long peakDay, String sentiment,
			Integer limit, Integer offset) {
		Query query = new Query(Criteria.where("peakDay").is(peakDay)
				.andOperator(Criteria.where("sentiment").is(stringToSentimentInt(sentiment))));
		query.skip(offset).limit(limit);
		List<WebData> list = mongoTemplate.find(query, WebData.class, getCollection(project));
		return toListArticle(list);
	}
	
	public int stringToSentimentInt(String sentiment){
		if(sentiment.equalsIgnoreCase("positive") || sentiment.equalsIgnoreCase("1"))
			return WebData.POSITIVE_SENTIMENT;
		if(sentiment.equalsIgnoreCase("negative") || sentiment.equalsIgnoreCase("-1"))
			return WebData.NEGATIVE_SENTIMENT;
		return WebData.NEUTRAL_SENTIMENT;
	}

	@Override
	public List<PeakCounter> getPeakDayMedia(Project project, Date start, Date end) {
		//TODO
		return null;
	}

	@Override
	public void deleteArticleByProjectId(Project project, String urlId) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("_id").is(urlId));
		WebData webData = mongoTemplate.findOne(query, WebData.class, getCollection(project));
		log.info(webData.getUrl());
		mongoTemplate.remove(webData, getCollection(project));
	}
	
	@Override
	public ArticleData editArticleByProjectId(Project project, String urlId) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("_id").is(urlId));
		WebData webData = mongoTemplate.findOne(query, WebData.class, getCollection(project));
		return webData.toArticle();
	}
	
	@Override
	public ArticleData getSingleArticleById(Project project, String urlId) {
		createCollectionIfNotExist(getCollection(project));
		Query query = new Query(Criteria.where("_id").is(urlId));
		WebData webData = mongoTemplate.findOne(query, WebData.class, getCollection(project));
		return webData.toArticle();
	}
	
	@Override
	public List<TermCounter> getKeywordMappingNew(Project project, Date startDate, Date endDate) {
		createCollectionIfNotExist(getCollectionNew(KEYWORD_MAPPING_COLLECTION));
			String map = "function(){" 
						   + "for(var i=0; i < this.termCounter.length; i++){"
						      + "emit(this.termCounter[i]._id, this.termCounter[i].value);"     
						    + "}"
						+"}";

			String reduce = "function(key,values){"
							    +"return Array.sum(values);"
							+"}";
			
			Query query = new Query(Criteria.where("date").gte(startDate).lte(endDate));
			query.addCriteria(Criteria.where("projectId").is(project.getId()));
			MapReduceResults<TermCounter> results = mongoTemplate.mapReduce(query, getCollectionNew(KEYWORD_MAPPING_COLLECTION), map, reduce,
					TermCounter.class);
			
			List<TermCounter> termCounter = new ArrayList<>();
			for(TermCounter term : results){		
				termCounter.add(term);
			}
			Collections.sort(termCounter);
			if(termCounter.size() > MAX_RELATED_TERMS){
				termCounter = termCounter.subList(0, MAX_RELATED_TERMS);
			}
			return termCounter;
	}
	
}

package io.gnews.pro.web.request;

public class PaymentMethodForm {

	private String paymentId;
	private String method;
	
	public PaymentMethodForm() {
	
	}
	
	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}

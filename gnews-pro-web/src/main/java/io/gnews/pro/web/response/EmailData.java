package io.gnews.pro.web.response;

import io.gnews.pro.core.model.mongodb.EmailRecipient;

public class EmailData {

	private String email;
	private String firstName;
	private String lastName;
	
	public EmailData() {
	
	}

	public EmailData(EmailRecipient recipient) {
		this.setEmail(recipient.getEmail());
		this.setFirstName(recipient.getFirstName());
		this.setLastName(recipient.getLastName());
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}

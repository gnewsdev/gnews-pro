package io.gnews.pro.web.service;

import java.security.Principal;

import org.springframework.web.multipart.MultipartFile;

import io.gnews.pro.core.model.mongodb.UserAccount;
import io.gnews.pro.web.exception.EmailRegisteredException;
import io.gnews.pro.web.request.ChangePasswordForm;
import io.gnews.pro.web.request.InquiriesForm;
import io.gnews.pro.web.request.ProfileForm;
import io.gnews.pro.web.request.RegistrationForm;
import io.gnews.pro.web.response.ProfileData;

public interface UserService {

	public void savePassword(ChangePasswordForm form, Principal principal) throws Exception;

	public ProfileData getUserData(Principal principal);

	public void reuestInquiry(InquiriesForm form);

	public UserAccount findUserByEmail(String email);

	public void save(Principal principal, ProfileForm form, MultipartFile imgUpload);

	public void register(Principal principal, RegistrationForm form) throws EmailRegisteredException;
	
}

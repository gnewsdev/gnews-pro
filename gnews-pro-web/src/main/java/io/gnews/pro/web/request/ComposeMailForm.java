package io.gnews.pro.web.request;

import org.springframework.web.multipart.MultipartFile;

public class ComposeMailForm {

	private String senderId;
	private String groupId;
	private String subject;
	private String content;
	private MultipartFile[] file;
	
	public ComposeMailForm() {

	}
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public MultipartFile[] getFile() {
		return file;
	}

	public void setFile(MultipartFile[] file) {
		this.file = file;
	}

	public String getSenderId() {
		return senderId;
	}
	
}

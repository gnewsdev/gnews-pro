package io.gnews.pro.web.response;

public class PromoCodeData {
	
	private String promocode;
	private Long price;
	private int period;
	
	public PromoCodeData() {
	
	}

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

}

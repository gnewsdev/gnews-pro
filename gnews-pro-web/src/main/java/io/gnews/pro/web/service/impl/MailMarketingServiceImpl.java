package io.gnews.pro.web.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.base.Throwables;

import io.gnews.pro.core.config.RabbitMQConfig;
import io.gnews.pro.core.model.dto.MailType;
import io.gnews.pro.core.model.dto.MessageSimple;
import io.gnews.pro.core.model.dto.SendEmailForm;
import io.gnews.pro.core.model.mongodb.EmailGroup;
import io.gnews.pro.core.model.mongodb.EmailMessage;
import io.gnews.pro.core.model.mongodb.EmailReceiver;
import io.gnews.pro.core.model.mongodb.EmailRecipient;
import io.gnews.pro.core.model.mongodb.EmailSender;
import io.gnews.pro.core.model.mongodb.GroupRecipient;
import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.repository.mongodb.EmailGroupRepository;
import io.gnews.pro.core.repository.mongodb.EmailMessageRepository;
import io.gnews.pro.core.repository.mongodb.EmailReceipientRepository;
import io.gnews.pro.core.repository.mongodb.EmailReceiverRepository;
import io.gnews.pro.core.repository.mongodb.EmailSenderRepository;
import io.gnews.pro.core.repository.mongodb.GroupReceipientRepository;
import io.gnews.pro.web.request.AddEmailRecipient;
import io.gnews.pro.web.request.AddEmailSenderForm;
import io.gnews.pro.web.request.ComposeMailForm;
import io.gnews.pro.web.request.CreateGroupForm;
import io.gnews.pro.web.service.MailMarketingService;
import io.gnews.pro.web.service.ProjectService;
import io.gnews.pro.web.service.VerificationService;
import io.gnews.pro.web.util.RegexUtil;
import io.gnews.pro.web.util.URLUtils;
import io.gnews.zimbra.client.DefaultZimbraClient;
import io.gnews.zimbra.client.ZimbraClient;

@Service
@Transactional
public class MailMarketingServiceImpl implements MailMarketingService {

	private Logger log = LoggerFactory.getLogger(getClass());

	public static final String DEFAULT_PASSWORD_PREFIX = "!@()";
	
	public static final String[] Extension = {"csv","xls","xlsx"};

	@Value("${mail.attachment.folder}")
	private String baseFolder;
	@Value("${zimbra.soap.api.url}")
	private String zimbraUrl;
	@Value("${zimbra.soap.api.username}")
	private String zimbraUsername;
	@Value("${zimbra.soap.api.password}")
	private String zimbraPassword;
	
	@Autowired
	private EmailGroupRepository emailGroupRepository;

	@Autowired
	private EmailSenderRepository emailSenderRepository;

	@Autowired
	private EmailMessageRepository emailMessageRepository;

	@Autowired
	private EmailReceiverRepository emailReceiverRepository;

	@Autowired
	private EmailReceipientRepository emailRecipientRepository;

	@Autowired
	private GroupReceipientRepository groupRecipientRepository;
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private VerificationService verificationService;

	@Autowired
	private TemplateEngine templateEngine;
	
	private Set<String> importXLSContact(EmailGroup group, InputStream file){
		Set<String> set = new HashSet<>();
		try {
			Workbook wb = WorkbookFactory.create(file);
			Sheet mySheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = mySheet.rowIterator();
			List<EmailRecipient> emailRecipients = new ArrayList<>();

			while (rowIterator.hasNext()) {
				EmailRecipient emailReceipient = new EmailRecipient();

				Row row = rowIterator.next();
				String email = row.getCell(0).getRichStringCellValue().toString();
				String name = row.getCell(1).getRichStringCellValue().toString();
				String firstName = row.getCell(2).getRichStringCellValue().toString();
				String lastName = row.getCell(3).getRichStringCellValue().toString();

				emailReceipient.setEmail(email);
				emailReceipient.setName(name);
				emailReceipient.setFirstName(firstName);
				emailReceipient.setLastName(lastName);

				if (name == "") {
					emailReceipient.setName(firstName + " " + lastName);
				}
				if (firstName == "") {
					emailReceipient.setFirstName(name.substring(0, name.indexOf(' ')));
				}
				if (lastName == "") {
					emailReceipient.setLastName(name.substring(name.indexOf(' ')).trim());
				}
				emailRecipients.add(emailReceipient);

			}
			for (EmailRecipient e : emailRecipients) {
				if (e.getEmail().equals("email")) {
					continue;
				}
				addEmailReceipient(group, e);
				log.info(e.getEmail());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		return set;
	}
	
	private Set<String> importCSVContact(EmailGroup group, InputStream inputStream) {
		Set<String> set = new HashSet<String>();
		try {
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new InputStreamReader(inputStream));
			boolean hasHeader = false;
			int emailIndex = -1;
			int nameIndex = -1;
			int firstNameIndex = -1;
			int lastNameIndex = -1;
			int row = 0;

			for (CSVRecord record : records) {
				EmailRecipient emailReceipient = new EmailRecipient();
				if(hasHeader){
					if(emailIndex != -1){
						if (RegexUtil.patternMatcher(record.get(emailIndex), RegexUtil.EMAIL_PATTERN)) {
							set.add(record.get(emailIndex));
							emailReceipient.setEmail(record.get(emailIndex));
						}
					}
					if(nameIndex != -1){
						if(!record.get(nameIndex).equalsIgnoreCase("NULL"))
							emailReceipient.setName(record.get(nameIndex));
					}
					if(firstNameIndex != -1){
						if(!record.get(nameIndex).equalsIgnoreCase("NULL"))
							emailReceipient.setFirstName(record.get(firstNameIndex));
					}
					if(lastNameIndex != -1){
						if(!record.get(nameIndex).equalsIgnoreCase("NULL"))
							emailReceipient.setLastName(record.get(lastNameIndex));						
					}
				}else{
					for (int i = 0; i < record.size(); i++) {
						String column = record.get(i).trim();
						if (RegexUtil.patternMatcher(column, RegexUtil.EMAIL_PATTERN)) {
							set.add(column);
							emailReceipient.setEmail(column);
						} else {
							if (RegexUtil.patternMatcher(column, RegexUtil.FULLNAME_PATTERN)
									|| RegexUtil.patternMatcher(column, RegexUtil.FULLNAME_PATTERN2)) {
								if (emailReceipient.getName() == null) {
									emailReceipient.setName(column);
								} else if (emailReceipient.getFirstName() == null) {
									emailReceipient.setFirstName(column);
								} else if (emailReceipient.getLastName() == null) {
									emailReceipient.setLastName(column);
								}
							}
						}
					}
				}
				
				if (emailReceipient.getEmail() != null) {
					addEmailReceipient(group, emailReceipient);
				} else {
					if (row == 0) {
						hasHeader = true;
						for (int x = 0; x < record.size(); x++) {
							if (record.get(x).trim().toLowerCase().contains("email")) {
								emailIndex = x;
							} else if (record.get(x).trim().toLowerCase().contains("first")
									&& record.get(x).trim().toLowerCase().contains("name")) {
								firstNameIndex = x;
							} else if (record.get(x).trim().toLowerCase().contains("last")
									&& record.get(x).trim().toLowerCase().contains("name")) {
								lastNameIndex = x;
							} else if (record.get(x).trim().toLowerCase().contains("name")) {
								nameIndex = x;
							}
						}
					}
				}
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return set;
	}

	public void addEmailReceipient(EmailGroup group, EmailRecipient recipient) {
		try{
			GroupRecipient groupRecipient = groupRecipientRepository.findByGroupAndRecipient(
					group, 
					saveEmailReceipient(recipient));
			if(groupRecipient == null){
				groupRecipient = new GroupRecipient();
				groupRecipient.setGroup(group);
				groupRecipient.setRecipient(recipient);
				groupRecipientRepository.save(groupRecipient);
				group.setMemberCount(new Long(groupRecipientRepository.countByGroup(group)).intValue());
				emailGroupRepository.save(group);
			}
		}catch(Exception e){
			log.error(Throwables.getStackTraceAsString(e));
		}
	}

	private EmailRecipient saveEmailReceipient(EmailRecipient emailRecipient){
		EmailRecipient recipient = emailRecipientRepository.findByEmail(emailRecipient.getEmail());
		if(recipient == null){
			if(emailRecipient.getName() != null){
				if(emailRecipient.getFirstName() == null){
					emailRecipient.setFirstName(emailRecipient.getName().split(" ")[0]);
				}
				if(emailRecipient.getLastName() == null){
					emailRecipient.setLastName(emailRecipient.getName().substring(
							emailRecipient.getFirstName().length(), 
							emailRecipient.getName().length()));
				}
			}else{
				if(emailRecipient.getFirstName() != null){
					emailRecipient.setName(emailRecipient.getFirstName());
				}
				if(emailRecipient.getLastName() != null){
					emailRecipient.setName(emailRecipient.getName() + " " + emailRecipient.getLastName());
				}
			}
			return emailRecipientRepository.save(emailRecipient);
		}else{
			return recipient;
		}
	}
	
	private EmailSender saveSender(Project project, AddEmailSenderForm form) {
		EmailSender sender = emailSenderRepository.findByProjectIdAndEmail(project.getId(), form.getEmail());
		if (sender == null) {
			sender = new EmailSender();
			sender.setEmail(form.getEmail());
			sender.setDisplayName(form.getName());
			sender.setProjectId(project.getId());
			sender = emailSenderRepository.save(sender);
		}
		return sender;
	}

	@Override
	public void addSender(HttpServletRequest request, Principal principal, Project project, AddEmailSenderForm form) {
		EmailSender sender = saveSender(project, form);
		SendEmailForm emailForm = new SendEmailForm();
		emailForm.setTo(sender.getEmail());
		emailForm.setType(MailType.VALIDATE);
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("validationUrl", verificationService.generateSenderVerificationUrl(sender, request));
			emailForm.setData(map);
			rabbitTemplate.convertAndSend(RabbitMQConfig.SEND_MAIL_QUEUE, emailForm);
		}catch(Exception e){
			log.error(e.getMessage());
		}
	}

	@Override
	public void editSender(Principal principal, Project project, AddEmailSenderForm form) {
		saveSender(project, form);
	}
	
	@Override
	public List<EmailMessage> getBlastData(Project project, Integer page, Integer size) {
		if (page == null)
			page = 0;
		if (size == null)
			size = 10;
		PageRequest pageRequest = new PageRequest(page, size, new Sort(Sort.Direction.DESC, "sendDate"));
		List<EmailMessage> list = emailMessageRepository.findByProjectId(project.getId(), pageRequest).getContent();
		for(EmailMessage message : list){
			String content = Jsoup.parse(message.getContent()).text();
			message.setContent(content);
		}
		return list;
	}

	@Override
	public List<EmailGroup> getMailGroups(Project project) {
		return emailGroupRepository.findByProjectId(project.getId());
	}

	@Override
	public EmailMessage getBlastDetail(String messageId) {
		return emailMessageRepository.findOne(messageId);
	}

	@Override
	public void saveMailBlast(Principal principal, Project project, ComposeMailForm form) {
		EmailGroup emailGroup = emailGroupRepository.findOne(form.getGroupId());
		EmailSender emailSender = emailSenderRepository.findOne(form.getSenderId());
		String messageId = UUID.randomUUID().toString();
		List<String> attachments = new ArrayList<>();
		if(form.getFile() != null){
			for (MultipartFile file : form.getFile()) {
				File f = new File(baseFolder + File.separator + messageId + File.separator + file.getName());
				try {
					file.transferTo(f);
					attachments.add(f.getAbsolutePath());
				} catch (IllegalStateException e) {
					log.error(e.getMessage());
				} catch (IOException e) {
					log.error(e.getMessage());
				}
			}
		}
		saveAndSendEmail(project, emailGroup, emailSender, form.getSubject(), form.getContent(), attachments);
	}

	@Override
	public void saveMailBlast(MessageSimple message) {
		log.info("receiving email from listener : " + message.getMessageId() );
		for (String to : message.getEmailTo()) {
			sendEmail(message.getEmailFrom(), to, message.getSendDate(), message.getSubject(), message.getContent(),
					message.getAttachments());
		}
		for (String to : message.getEmailCc()) {
			sendEmail(message.getEmailFrom(), to, message.getSendDate(), message.getSubject(), message.getContent(),
					message.getAttachments());
		}
		for (String to : message.getEmailBcc()) {
			sendEmail(message.getEmailFrom(), to, message.getSendDate(), message.getSubject(), message.getContent(),
					message.getAttachments());
		}
	}

	private void sendEmail(String from, String to, Date date, String subject, String content,
			List<String> attachments) {
		EmailReceiver emailReceiver = emailReceiverRepository.findByEmail(to);
		EmailGroup group = emailGroupRepository.findByEmailReceiver(emailReceiver);
		EmailSender emailSender = emailSenderRepository.findByEmail(from);
		Project project = projectService.getById(group.getProjectId());
		if (emailReceiver != null) {
			saveAndSendEmail(project, group, emailSender, subject, content, attachments);
		}
	}

	private void saveAndSendEmail(Project project, EmailGroup emailGroup, EmailSender emailSender, String subject, String content, List<String> attachments){
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setProjectId(project.getId());
		emailMessage.setContent(content);
		emailMessage.setSubject(subject);
		emailMessage.setEmailGroup(emailGroup);
		emailMessage.setSender(emailSender);
		emailMessage.setMessageId(UUID.randomUUID().toString());
		emailMessage.setSendDate(new Date());
		emailMessage.setAttachments(attachments);
		Context ctx = new Context();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("content", content);
		ctx.setVariables(map);
		String htmlContent = templateEngine.process("mail/campaign-default", ctx);
		emailMessage.setHtmlContent(htmlContent);
		emailMessage = emailMessageRepository.save(emailMessage);
		rabbitTemplate.convertAndSend(RabbitMQConfig.DISTRIBUTE_MAIL_BLAST_QUEUE, emailMessage.getId());
	}
	@Override
	public EmailGroup getMailGroup(String groupId) {
		return emailGroupRepository.findOne(groupId);
	}

	@Override
	public List<EmailRecipient> getMailRecipients(String groupId, Integer page, Integer size) {
		if (page == null)
			page = 0;
		if (size == null)
			size = 10;
		PageRequest pageRequest = new PageRequest(page, size);
		List<EmailRecipient> list = new ArrayList<>();
		EmailGroup group = emailGroupRepository.findOne(groupId);
		List<GroupRecipient> groupReceipients  = groupRecipientRepository.findByGroup(group, pageRequest).getContent();
		log.info("receipient count : " + groupReceipients.size());
		for(GroupRecipient groupReceipient : groupReceipients){
			list.add(groupReceipient.getRecipient());
		}
		return list;
	}

	@Override
	public void deleteGroup(Principal principal, String groupId) {
		emailGroupRepository.delete(groupId);
	}

	@Override
	public void addReceipient(Principal principal, String groupId, AddEmailRecipient form) {
		EmailGroup emailGroup = emailGroupRepository.findOne(groupId);
		EmailRecipient emailReceipient = new EmailRecipient();
		emailReceipient.setEmail(form.getEmail());
		emailReceipient.setFirstName(form.getFirstName());
		emailReceipient.setLastName(form.getLastName());
		addEmailReceipient(emailGroup, emailReceipient);
	}

	@Override
	public List<EmailSender> getEmailSender(String projectId) {
		Project project = projectService.getById(projectId);
		return emailSenderRepository.findByProjectId(project.getId());
	}

	@Override
	public void deleteSender(Principal principal, Project project, String senderId) {
		emailSenderRepository.delete(senderId);
	}
	
	@Override
	public void addGroupMail(Principal principal, Project project, CreateGroupForm form) {
		EmailGroup group = new EmailGroup();
		group.setProjectId(project.getId());
		group.setName(form.getName());
		group.setDescription(form.getDescription());
		group = emailGroupRepository.save(group);
		EmailReceiver emailReceiver = createEmailReceiver(group, form.getEmailReceiver());
		group.setEmailReceiver(emailReceiver);
		emailGroupRepository.save(group);
		importContact(group, form.getFile());
	}

	@Override
	public void importContact(final EmailGroup group, MultipartFile file) {
		log.info("start import contact");
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		log.info(extension + "ini extensinya");
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					if (extension.equals(Extension[0])) {
						importCSVContact(group, file.getInputStream());
						log.info("finish import contact CSV");
					} else if (extension.equals(Extension[1]) || extension.equals(Extension[2])) {
						importXLSContact(group, file.getInputStream());
						log.info("finish import contact xls");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private EmailReceiver createEmailReceiver(EmailGroup emailGroup, String emailReceiver) {
		ZimbraClient zimbraClient = new DefaultZimbraClient(zimbraUrl, HttpClientBuilder.create().build());
		String email = generateEmail(emailReceiver);
		String password = email + DEFAULT_PASSWORD_PREFIX;
		if (zimbraClient.auth(zimbraUsername, zimbraPassword)) {
			if (!zimbraClient.userExist(email)) {
				if (zimbraClient.createAccount(email, password, emailGroup.getName(), "group")) {
					EmailReceiver emailReceiver1 = new EmailReceiver();
					emailReceiver1.setEmail(email);
					emailReceiver1.setPassword(password);
					return emailReceiverRepository.save(emailReceiver1);
				} else {
					log.error("unable to create account");
				}
			}
		} else {
			log.error("unable to auth zimbra");
		}
		return null;
	}

	private String generateEmail(String email) {
		String emailSuffix = "@gnews.id";
		String emailTest = email.trim().toLowerCase() + emailSuffix;
		log.info(emailTest);
		return emailTest;
	}

	@Override
	public List<EmailSender> getValidEmailSender(Project project) {
		return emailSenderRepository.findByProjectIdAndValidTrue(project.getId());
	}

	@Override
	public void deleteReceipient(Principal principal, String groupId, String receiverId) {
		EmailRecipient recipient = emailRecipientRepository.findOne(receiverId);
		EmailGroup emailGroup = emailGroupRepository.findOne(groupId);
		if(emailGroup == null) return;
		if(recipient == null) return;
		GroupRecipient groupRecipient = groupRecipientRepository.findByGroupAndRecipient(emailGroup, recipient);
		if(groupRecipient != null){
			groupRecipientRepository.delete(groupRecipient);
			emailGroup.setMemberCount(emailGroup.getMemberCount() - 1);
			emailGroupRepository.save(emailGroup);
		}
	}

	@Override
	public void unsubscribeReceipient(HttpServletRequest request, String messageId, String recipientId) {
		EmailRecipient recipient = emailRecipientRepository.findOne(recipientId);
		EmailMessage emailMessage = emailMessageRepository.findOne(messageId);
		EmailGroup emailGroup = emailMessage.getEmailGroup();
		if(emailGroup == null) return;
		if(recipient == null) return;
		GroupRecipient groupRecipient = groupRecipientRepository.findByGroupAndRecipient(emailGroup, recipient);
		if(groupRecipient != null){
			groupRecipient.setUnsubscribe(true);
			groupRecipientRepository.save(groupRecipient);
			emailGroup.setMemberCount(emailGroup.getMemberCount() - 1);
			emailGroupRepository.save(emailGroup);
		}
	}

	@Override
	public String generateUnsubscribeUrl(HttpServletRequest request, String messageId, String recipientId) {
		return URLUtils.getBaseURl(request) + "/mail/unsubscribe/" + messageId + "/" + recipientId;
	}

}

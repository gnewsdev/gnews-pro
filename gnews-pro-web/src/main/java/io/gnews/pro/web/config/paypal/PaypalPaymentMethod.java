package io.gnews.pro.web.config.paypal;

public enum PaypalPaymentMethod {

	credit_card, paypal
	
}

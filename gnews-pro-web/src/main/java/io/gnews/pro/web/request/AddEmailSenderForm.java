package io.gnews.pro.web.request;

public class AddEmailSenderForm {

	private String name;
	private String email;
	
	public AddEmailSenderForm() {
	
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}

package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.EmailGroup;
import io.gnews.pro.core.model.mongodb.EmailReceiver;

public interface EmailGroupRepository extends MongoRepository<EmailGroup, String> {

	public List<EmailGroup> findByProjectId(String projectId);

	public EmailGroup findByEmailReceiver(EmailReceiver emailReceiver);

}

package io.gnews.pro.core.repository.mongodb;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.Project;
import io.gnews.pro.core.model.mongodb.UserAccount;

public interface ProjectRepository extends MongoRepository<Project, String> {

	public List<Project> findByUserAndDeletedFalse(UserAccount user);

	public List<Project> findByUserAndStatusAndDeletedFalse(UserAccount user, String projectStatus);

	public List<Project> findByUserAndActiveTrue(UserAccount user);
	
	public List<Project> findByActiveTrue();
	
	public List<Project> findByActiveTrueAndDayAndHour(String day, int hour);

	public List<Project> findByExpiredAt(Date date);
	
	public Project findByIdAndHour(String id, Long Hour);
	
}

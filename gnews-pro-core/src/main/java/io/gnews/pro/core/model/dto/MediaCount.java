package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class MediaCount {

	private Long totalMedia;
	private Long totalArticle;
	
	public MediaCount() {
	
	}

	public Long getTotalMedia() {
		return totalMedia;
	}

	public void setTotalMedia(Long totalMedia) {
		this.totalMedia = totalMedia;
	}

	public Long getTotalArticle() {
		return totalArticle;
	}

	public void setTotalArticle(Long totalArticle) {
		this.totalArticle = totalArticle;
	}
	
}

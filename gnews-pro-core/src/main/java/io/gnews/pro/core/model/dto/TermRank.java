package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 4, 2015
 */
public class TermRank {

	private String term;
	private int rank;
	
	public TermRank() {

	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
}

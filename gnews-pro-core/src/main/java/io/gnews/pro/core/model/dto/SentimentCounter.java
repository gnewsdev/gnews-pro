package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class SentimentCounter {

	private Long positive;
	private Long negative;
	private Long neutral;
	
	public SentimentCounter() {
		// TODO Auto-generated constructor stub
	}

	public Long getPositive() {
		return positive;
	}

	public void setPositive(Long positive) {
		this.positive = positive;
	}

	public Long getNegative() {
		return negative;
	}

	public void setNegative(Long negative) {
		this.negative = negative;
	}

	public Long getNeutral() {
		return neutral;
	}

	public void setNeutral(Long neutral) {
		this.neutral = neutral;
	}
	
}

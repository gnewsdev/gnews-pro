package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import io.gnews.pro.core.config.CascadeSave;

@SuppressWarnings("serial")
@Document(collection = "group_recipient")
public class GroupRecipient implements Serializable {

	@Id
	private String id;
	@DBRef
	@CascadeSave
	private EmailRecipient recipient;
	@DBRef
	@CascadeSave
	private EmailGroup group;
	@DBRef
	@CascadeSave
	private EmailGroup parentGroup;
	private boolean unsubscribe = false;
	
	public GroupRecipient() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public EmailRecipient getRecipient() {
		return recipient;
	}

	public void setRecipient(EmailRecipient recipient) {
		this.recipient = recipient;
	}
	
	public boolean isUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public EmailGroup getGroup() {
		return group;
	}

	public void setGroup(EmailGroup group) {
		this.group = group;
	}

	public EmailGroup getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(EmailGroup parentGroup) {
		this.parentGroup = parentGroup;
	}
	
}

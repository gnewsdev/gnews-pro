package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gnews.pro.core.model.mongodb.GnewsLetter;
import io.gnews.pro.core.model.mongodb.Project;

public interface GnewsLetterRepository extends MongoRepository<GnewsLetter, String>{
	
	public List<GnewsLetter> findByProject(Project project);
	
	public List<GnewsLetter> findByProjectId(String id);
	
	public List<GnewsLetter> findByProjectExpiredTrue();
	
	public List<GnewsLetter> findByHourAndActiveTrue(int hour);

}

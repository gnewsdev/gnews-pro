package io.gnews.pro.core.model.dto;

/**
 * simple class helper for article response
 * 
 * @author masasdani
 * Created Date Oct 27, 2015
 */
public class Article {

	private String id;
	private String url;
	private String host;
	private String twitterUsername;
	private String title;
	private String image;
	private String favicon;
	private Long date;
	private String content;
	private int totalFavorite;
	private int totalRead;
	private boolean userLiked;
	private boolean userArchived;
	private boolean useRead;

	public Article() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isUserLiked() {
		return userLiked;
	}

	public void setUserLiked(boolean userLiked) {
		this.userLiked = userLiked;
	}

	public boolean isUserArchived() {
		return userArchived;
	}

	public void setUserArchived(boolean userArchived) {
		this.userArchived = userArchived;
	}

	public String getTwitterUsername() {
		return twitterUsername;
	}

	public void setTwitterUsername(String twitterUsername) {
		this.twitterUsername = twitterUsername;
	}
	
	public int getTotalFavorite() {
		return totalFavorite;
	}

	public void setTotalFavorite(int totalFavorite) {
		this.totalFavorite = totalFavorite;
	}

	public int getTotalRead() {
		return totalRead;
	}

	public void setTotalRead(int totalRead) {
		this.totalRead = totalRead;
	}

	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public boolean isUseRead() {
		return useRead;
	}

	public void setUseRead(boolean useRead) {
		this.useRead = useRead;
	}
	
}

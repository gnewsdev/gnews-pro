package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * class for storing user data in jpa
 * 
 * @author madqori Created Date Jan 22, 2016
 */
@SuppressWarnings("serial")
@Document(collection = "user_account")
public class UserAccount implements Serializable{
	
	@Id
	private String id;
	private String fullname;
	private String company;
	private String address;
	private String country;
	private String profilePicture;
	private boolean recurrentUser = true;
	@DBRef
	private Account account;
	private Long phone;
	private String position;
	private String city;
	
	public UserAccount() {
	
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}


	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public boolean isRecurrentUser() {
		return recurrentUser;
	}

	public void setRecurrentUser(boolean recurrentUser) {
		this.recurrentUser = recurrentUser;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}

package io.gnews.pro.core.model.mongodb;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * class for storing user data in jpa
 * 
 * @author madqori Created Date Jan 22, 2016
 */
@SuppressWarnings("serial")
@Document(collection = "account")
public class Account implements Serializable {

	public static final String TYPE_USER = "USER";
	public static final String TYPE_ADMIN = "ADMIN";
	public static final String TYPE_AGENT = "AGENT";
	public static final String TYPE_TEAM  = "TEAM";
	public static final String ACCESSTOKEN = "aHCl8LjxTPAE3bsLcLM0";
	
	@Id
	private String id;
	@Indexed(unique = true)
	private String username;
	private String password; 
	@Indexed(unique = true)
	private String email;
	private Date createdAt;
	private Date lastUpdate;
	private boolean canLogin=true;
	private Date lastLogin;
	private String type = TYPE_USER;
	private boolean active = true;
	
	public Account() {
		
	}

	public Account(String email, boolean canLogin, String username, String password) {
		this.canLogin = canLogin;
		this.username = username;
		this.email = email;
		this.password = password;
		this.setCreatedAt(new Date());
		this.setLastUpdate(new Date());
	}
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isCanLogin() {
		return canLogin;
	}

	public void setCanLogin(boolean canLogin) {
		this.canLogin = canLogin;
	}

}

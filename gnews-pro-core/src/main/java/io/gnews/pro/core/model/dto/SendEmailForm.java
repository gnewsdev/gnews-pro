package io.gnews.pro.core.model.dto;

import java.util.Map;

public class SendEmailForm {

	private String to;
	private MailType type;
	private Map<String, Object> data;
	private String[] attachments;
	
	public SendEmailForm() {
	
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public MailType getType() {
		return type;
	}

	public void setType(MailType type) {
		this.type = type;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String[] getAttachments() {
		return attachments;
	}

	public void setAttachments(String[] attachments) {
		this.attachments = attachments;
	}
	
}

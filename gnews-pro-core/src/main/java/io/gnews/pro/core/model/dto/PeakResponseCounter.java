package io.gnews.pro.core.model.dto;

/**
 * @author masasdani
 * Created Date Nov 3, 2015
 */
public class PeakResponseCounter {
	
	private Long id;	
	private Long count;
	private Long response;
	private Long precentage;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCount() {
		return count;
	}
	
	public Long getResponse() {
		return response;
	}
	
	public void setCount(Long count) {
		this.count = count;
	}
	
	public void setResponse(Long response) {
		this.response = response;
	}

	public Long getPrecentage() {
		return precentage;
	}

	public void setPrecentage(Long precentage) {
		this.precentage = precentage;
	}
	
	

	/*
	public class Value {
	
		private Long count;
		private Long response;
		
		public Long getCount() {
			return count;
		}
		
		public Long getResponse() {
			return response;
		}
		
		public void setCount(Long count) {
			this.count = count;
		}
		
		public void setResponse(Long response) {
			this.response = response;
		}
		
	}*/
	
}

package io.gnews.pro.core.model.mongodb;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "media_ranking_global")
public class MediaRankingGlobal {
	
	@Id
	private String id;
	private String domain;
	private int globalRank;
	private DateTime date;
	private String countryName;
	private int countryRank;
	
	public MediaRankingGlobal() {
		// TODO Auto-generated constructor stub
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getGlobalRank() {
		return globalRank;
	}

	public void setGlobalRank(int globalRank) {
		this.globalRank = globalRank;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getCountryRank() {
		return countryRank;
	}

	public void setCountryRank(int countryRank) {
		this.countryRank = countryRank;
	}

}

package io.gnews.pro.core.repository.mongodb;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import io.gnews.pro.core.model.mongodb.EmailGroup;
import io.gnews.pro.core.model.mongodb.EmailRecipient;
import io.gnews.pro.core.model.mongodb.GroupRecipient;

public interface GroupReceipientRepository extends PagingAndSortingRepository<GroupRecipient, String> {

	public GroupRecipient findByGroupAndRecipient(EmailGroup group, EmailRecipient receipient);

	public List<GroupRecipient> findByGroup(EmailGroup group);

	public Page<GroupRecipient> findByGroup(EmailGroup group, Pageable pageable);

	public List<GroupRecipient> findByGroupAndUnsubscribe(EmailGroup group, boolean unsubscribe);

	public Page<GroupRecipient> findByGroupAndUnsubscribe(EmailGroup group, boolean unsubscribe, Pageable pageable);

	public long countByGroup(EmailGroup group);

}

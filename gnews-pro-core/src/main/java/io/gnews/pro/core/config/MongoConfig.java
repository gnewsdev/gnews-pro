package io.gnews.pro.core.config;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories(basePackages = "io.gnews.pro.core.repository.mongodb")
class MongoConfig extends AbstractMongoConfiguration {

	@Value("${mongodb.host}")
    private String host;
    @Value("${mongodb.port}")
    private Integer port;
    @Value("${mongodb.dbname}")
    private String dbname;
    @Value("${mongodb.username}")
    private String username;
    @Value("${mongodb.password}")
    private String password;
        
	@Bean 
	public Mongo mongo() throws UnknownHostException {
		return new MongoClient(this.host, this.port);
	}

	@Override
	protected String getDatabaseName() {
		return this.dbname;
	}
	
	@Bean
	public MongoCascadeListener mongoCascadeListener(){
		return new MongoCascadeListener();
	}

	@Override
	@Bean
	public SimpleMongoDbFactory mongoDbFactory() throws Exception {
	    return new SimpleMongoDbFactory(mongo(), getDatabaseName());
	}
	
	@Override
	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
	    final UserCredentials userCredentials = new UserCredentials(this.username, this.password);

	    final MongoTemplate mongoTemplate = new MongoTemplate(mongo(), getDatabaseName(), userCredentials);
	    mongoTemplate.setWriteConcern(WriteConcern.SAFE);

	    return mongoTemplate;
	}	
	
}

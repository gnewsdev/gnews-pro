package io.gnews.pro.cms.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.UserForm;
import io.gnews.pro.cms.model.response.UserData;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.cms.service.UserService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/client")
public class ClientController {

	@Autowired
	SiteService siteService;

	@Autowired
	UserService clientService;
	
	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<UserData> client = clientService.getUserData(principal);
		model.addAttribute("client", client);

		return "all-client";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/add")
	public String add(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		model.addAttribute("client", new UserData());

		return "new-client";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	public String edit(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		UserData client = clientService.getUserById(id);
		model.addAttribute("client", client);

		return "new-client";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public String delete(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		clientService.delete(id);

		return "redirect:/client/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute UserForm form) {
		if (principal == null) {
			return "redirect:/signin";
		}
		clientService.save(principal, form);
		
		return "redirect:/client/all";
	}
}

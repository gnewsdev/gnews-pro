package io.gnews.pro.cms.controller.admin;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.UserForm;
import io.gnews.pro.cms.model.response.UserData;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.cms.service.UserService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value = "/admin/user")
public class AdminUserController {

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;
	
	@Autowired
	AccountRepository accountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<UserData> user = userService.getUserData(principal);
		model.addAttribute("user", user);
		return "admin/all-user";
	}

	// not Yet implemented
	@RequestMapping(method = RequestMethod.GET, value = "/projects")
	public String projectlist(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<UserData> user = userService.getUserData(principal);
		model.addAttribute("user", user);
		return "admin/project-list";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/add")
	public String add(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		model.addAttribute("user", new UserData());
		return "admin/new-user";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/active")
	public String active(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		userService.markActive(id);
		return "redirect:/admin/user/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/suspend")
	public String suspend(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		userService.markSuspend(id);
		return "redirect:/admin/user/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute UserForm Form) {
		if (principal == null) {
			return "redirect:/signin";
		}
		userService.save(principal, Form);
		return "redirect:/admin/user/all";
	}
}

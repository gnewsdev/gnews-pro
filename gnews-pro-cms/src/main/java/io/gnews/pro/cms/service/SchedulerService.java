package io.gnews.pro.cms.service;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author madqori
 * Created Date Feb 16, 2016
 */
@Service
public class SchedulerService {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PromoCodeSchedulerService postSchedulerService;

	@PostConstruct
	public void init() {
		log.info("start scheduling service");
	}

	@Scheduled(cron = "0 0 0 * * ?")
	public void updatePostSchedule() {
		DateTime dateTime = new DateTime(new Date());
		log.info("Checking And Update Promo Code");
		postSchedulerService.updateAllData(dateTime.dayOfMonth().roundFloorCopy().toDate());
	}

}

package io.gnews.pro.cms.service;


import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;

import io.gnews.pro.cms.model.request.UserForm;
import io.gnews.pro.cms.model.response.UserData;

@Service
public interface UserService {

	/**
	 * @param principal
	 * @return
	 */
	public List<UserData> getUserData(Principal principal);
	
	/**
	 * @param principal
	 * @param form
	 */
	public void save(Principal principal,UserForm form);
	
	/**
	 * @param id
	 * @return
	 */
	public UserData getUserById(String id);

	/**
	 * @param id
	 */
	public void markActive(String id);
	
	/**
	 * @param id
	 */
	public void markSuspend(String id);

	/**
	 * @param id
	 */
	public void delete(String id);
	
}

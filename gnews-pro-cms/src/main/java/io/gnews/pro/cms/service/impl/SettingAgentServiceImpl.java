package io.gnews.pro.cms.service.impl;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.gnews.pro.cms.exception.PasswordNotMatchException;
import io.gnews.pro.cms.model.request.ChangePasswordForm;
import io.gnews.pro.cms.model.request.DefaultPriceForm;
import io.gnews.pro.cms.model.response.ProfileData;
import io.gnews.pro.cms.service.SettingService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;

@Service
public class SettingAgentServiceImpl implements SettingService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private AgentAccountRepository agentAccountRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public void save(ChangePasswordForm form, Principal principal) throws Exception {
		Account account = accountRepository.findByEmail(principal.getName());
		if(passwordEncoder.matches(form.getOldPassword(),account.getPassword())){
			account.setPassword(passwordEncoder.encode(form.getNewPassword()));
			accountRepository.save(account);
		}else{
			throw new PasswordNotMatchException();
		}
	}

	@Override
	public ProfileData getProfileById(String id) {
		Account account = accountRepository.findOne(id);
		AgentAccount profil  = agentAccountRepository.findByAccount(account);

		if (profil == null) {
			return null;
		}
		
		ProfileData profileData = new ProfileData();
		profileData.setAccountId(account.getId());
		profileData.setEmail(account.getEmail());
		profileData.setPassword(account.getPassword());
		
		profileData.setUserId(profil.getId());
		profileData.setDefaultPrice(profil.getDefaultPrice());
		profileData.setFullname(profil.getFullname());
		
		return profileData;
	}

	@Override
	public ProfileData getProfileByEmail(String email) {
		Account account = accountRepository.findByEmail(email);
		
		AgentAccount profil = agentAccountRepository.findByAccount(account);
		
		if (profil == null) {
			return null;
		}
		ProfileData profileData = new ProfileData();
		profileData.setAccountId(account.getId());
		profileData.setEmail(account.getEmail());
		profileData.setPassword(account.getPassword());
		
		profileData.setUserId(profil.getId());
		profileData.setFullname(profil.getFullname());
		profileData.setDefaultPrice(profil.getDefaultPrice());
		
		return profileData;
	}

	@Override
	public void savePrice(DefaultPriceForm form, Principal principal) {
		Account account = accountRepository.findByEmail(principal.getName());
		AgentAccount agentAccount = agentAccountRepository.findByAccount(account);
		
		agentAccount.setDefaultPrice(form.getDefaultPrice());
		agentAccountRepository.save(agentAccount);
	}

}

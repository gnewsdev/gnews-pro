package io.gnews.pro.cms.controller;

import java.security.Principal;
import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.base.Throwables;

import io.gnews.pro.cms.service.SiteService;

@Controller
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SiteService siteService;
	
	@RequestMapping(value = "/error")
	public String error(
			Principal principal, 
			Model model, HttpServletRequest request,
			HttpServletResponse response){
		siteService.setSiteInfo(principal, model);
		Integer statusCode = (Integer) request
				.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request
				.getAttribute("javax.servlet.error.exception");
		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		String message = MessageFormat.format("{0}, {1}", statusCode,
				exceptionMessage);
		log.error(message);
		return "error-page";
    }

	/**
	 * @param throwable
	 * @param statusCode
	 * @return
	 */
	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
	
}

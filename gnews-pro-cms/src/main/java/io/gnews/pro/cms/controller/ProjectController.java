package io.gnews.pro.cms.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.gnews.pro.cms.model.response.ProjectData;
import io.gnews.pro.cms.service.ProjectService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.repository.mongodb.AccountRepository;

@Controller
@RequestMapping(value="/project")

public class ProjectController {
	
	@Autowired
	SiteService siteService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	AccountRepository accountRepository;
	
	@RequestMapping(method = RequestMethod.GET, value ="/all")
	public String projectSubmission(Principal principal, Model model){
			if (principal == null) {
				return "redirect:/signin";
			}
			siteService.setSiteInfo(principal, model);
			Account account = accountRepository.findByEmail(principal.getName());
			model.addAttribute("sidebar",account.getType());
			List<ProjectData> project = projectService.getProjectData(principal);
			model.addAttribute("project", project);
		
			return "project-submission";
	}
}

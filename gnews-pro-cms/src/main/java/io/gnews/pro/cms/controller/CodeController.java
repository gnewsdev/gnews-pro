package io.gnews.pro.cms.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.gnews.pro.cms.model.request.PromoCodeForm;
import io.gnews.pro.cms.model.response.PromoCodeData;
import io.gnews.pro.cms.service.PromoCodeService;
import io.gnews.pro.cms.service.SiteService;
import io.gnews.pro.cms.util.RandomUtil;
import io.gnews.pro.core.model.mongodb.Account;
import io.gnews.pro.core.model.mongodb.AgentAccount;
import io.gnews.pro.core.repository.mongodb.AccountRepository;
import io.gnews.pro.core.repository.mongodb.AgentAccountRepository;

@Controller
@RequestMapping(value ="/code" )
public class CodeController {

	@Autowired
	SiteService siteService;

	@Autowired
	PromoCodeService createCodeService;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	AgentAccountRepository agentAccountRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/all")
	public String index(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		List<PromoCodeData> code = createCodeService.getCreateCodeData(principal);
		model.addAttribute("code", code);

		return "all-codes";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/add")
	public String add(Principal principal, Model model) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account accounts = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",accounts.getType());
		Account account = accountRepository.findByEmail(principal.getName());
		PromoCodeData createCodeData = new PromoCodeData();
		AgentAccount agent = agentAccountRepository.findByAccount(account);
		createCodeData.setCodename(agent.getCodename() + RandomUtil.getRandomWord(3) + RandomUtil.getRandomNum());
		model.addAttribute("code", createCodeData);
		return "create-code";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	public String edit(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		siteService.setSiteInfo(principal, model);
		Account account = accountRepository.findByEmail(principal.getName());
		model.addAttribute("sidebar",account.getType());
		PromoCodeData code = createCodeService.getCreateCodeById(id);
		model.addAttribute("code", code);

		return "create-code";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete")
	public String delete(Principal principal, Model model, @RequestParam String id) {
		if (principal == null) {
			return "redirect:/signin";
		}
		createCodeService.delete(id);
		
		return "redirect:/code/all";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public String add(Principal principal, @ModelAttribute PromoCodeForm Form) {
		if (principal == null) {
			return "redirect:/signin";
		}	
		createCodeService.save(principal, Form);
		
		return "redirect:/code/all";
	}

}

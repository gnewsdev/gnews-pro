
/**
 * Theme: Ubold Admin Template Author: Coderthemes SweetAlert
 */

!function($) {
    "use strict";

    var SweetAlert = function() {};

    // examples
    SweetAlert.prototype.init = function() {
        
    // Basic
    $('#sa-basic').click(function(){
        swal("Here's a message!");
    });

    // A title with a text under
    $('#sa-title').click(function(){
        swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis")
    });

    // Success Message
    $('#sa-success').click(function(){
        swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis", "success")
    });

    // Warning Message
    $('#sa-warning').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });
    
    	$(document).ready(function () {
    	$('body').on('click', '#sa-deleteclient', function() {
        var href = $(this).attr('name');
        console.log(href);
    	swal({   
            title: "Are you sure to delete this client?",   
            text: "You will not be able to recover this client after deletion",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f05050",   
            confirmButtonText: "Yes, delete it",   
            closeOnConfirm: false, 
            closeOnCancel: false
        }, function(isConfirm){   
        	if (isConfirm) {     
        		// swal("Deleted!", "Your client has been deleted.", "success");
        		$.ajax({
        			type: 'post',
        			url: href,
        			success: function(response){
        				window.location.href= "/client/all";
        			}
        		});
        		} 
        	else {     
        		swal("Cancelled", "", "error");   
        		}
        	});
    });
    	});
    	
    	$(document).ready(function () {
        	$('body').on('click', '#sa-deleteagent', function() {
            var href = $(this).attr('name');
            console.log(href);
        	swal({   
                title: "Are you sure to delete this agent?",   
                text: "You will not be able to recover this agent after deletion",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#f05050",   
                confirmButtonText: "Yes, delete it",   
                closeOnConfirm: false, 
                closeOnCancel: false
            }, function(isConfirm){   
            	if (isConfirm) {     
            		// swal("Deleted!", "Your client has been deleted.", "success");
            		$.ajax({
            			type: 'post',
            			url: href,
            			success: function(response){
            				window.location.href= "/admin/agent/all";
            			}
            		});
            		} 
            	else {     
            		swal("Cancelled", "", "error");   
            		}
            	});
        });
        	});
        
    	$(document).ready(function () {
        	$('body').on('click', '#sa-deletecode', function() {
            var href = $(this).attr('name');
            console.log(href);
        	swal({   
                title: "Are you sure to delete the code?",   
                text: "You will not be able to recover the code after deletion",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#f05050",   
                confirmButtonText: "Yes, delete it",   
                closeOnConfirm: false, 
                closeOnCancel: false
            }, function(isConfirm){   
            	if (isConfirm) {     
            		// swal("Deleted!", "Your client has been deleted.", "success");
            		$.ajax({
            			type: 'post',
            			url: href,
            			success: function(response){
            				window.location.href= "/code/all";
            			}
            		});
            		} 
            	else {     
            		swal("Cancelled", "", "error");   
            		}
            	});
        });
        	});

    // Parameter
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });

    // Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Sweet!",   
            text: "Here's a custom image.",   
            imageUrl: "assets/plugins/sweetalert/thumbs-up.jpg" 
        });
    });

    // Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });


    },
    // init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

// initializing
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);